<?php
/** @var string $input */
//$input = file_get_contents(__DIR__ . '/../../input/debug/03.txt');
$numbers = explode(chr(10), trim($input));

$calc = function ($numbers, $calcPrevalentBit) {
    for ($i = 0; $i < 12; $i++) {
        $count = 0;
        foreach ($numbers as $number) {
            if (str_split($number)[$i] == 1) {
                $count++;
            } else {
                $count--;
            }
        }

        $prevalentBit = $calcPrevalentBit($count);
        foreach ($numbers as $key => $number) {
            if ($prevalentBit != str_split($number)[$i]) {
                unset($numbers[$key]);
            }
            if (count($numbers) == 1) {
                return bindec(reset($numbers));
            }
        }
    }
    return 0;
};
$o2Rating = $calc($numbers, function ($count) {return ($count >= 0) ? 1 : 0;});
$co2Rating = $calc($numbers, function ($count) {return ($count >= 0) ? 0 : 1;});
dd($o2Rating*$co2Rating);
