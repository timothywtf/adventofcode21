<?php
/** @var string $input */
$numbers = explode(chr(10), trim($input));

$gamma = '';
$epsilon = '';
for ($i = 0; $i < 12; $i++) {
    $count = 0;
    foreach ($numbers as $number) {
        if (str_split($number)[$i] == 1) {
            $count++;
        } else {
            $count--;
        }
    }
    $gamma .= ($count > 0) ? '1' : '0';
    $epsilon .= ($count < 0) ? '1' : '0';
}

dump(bindec($epsilon) * bindec($gamma));
die;
