<?php
/** @var string $input */

//$input = <<<DEBUG
//Player 1 starting position: 4
//Player 2 starting position: 8
//DEBUG;

list($p1, $p2) = explode(chr(10), $input);
$array = str_split($p1);
$array2 = str_split($p2);

$positions = [
    1 => (int)array_pop($array),
    2 => (int)array_pop($array2)
];

$score = [
    1 => 0,
    2 => 0
];

$dieValue = 1;
$dieRolls = 0;
$toWin = 1000;
$turn = 1;

function move($start , $add)
{
    if (($start + $add) <= 10) {
        return $start + $add;
    }
    return (($start + $add) % 10 != 0) ? ($start + $add) % 10 : 10;
}


$gameEnded = false;
while(!$gameEnded) {
    $rolls = [1, 2, 3];
    $rollValueScore = 0;
    while (!empty($rolls)) {
        array_pop($rolls);
        $dieRolls++;
        $rollValueScore += $dieValue;
        if ($dieValue++ >= 100) {
            $dieValue = 1;
        }
    }

    $positions[$turn] = move($positions[$turn], $rollValueScore);
    $score[$turn] += $positions[$turn];

    echo sprintf(
        "Player %s rolls %s and moves to space %s for a total score of %s",
        $turn,
        $rollValueScore,
        $positions[$turn],
        $score[$turn]
    ) . PHP_EOL;

    if ($score[1] >= $toWin) {
        dump("Player 1 wins");
        dump($dieRolls * $score[2]);
        $gameEnded = true;
    }
    if ($score[2] >= $toWin) {
        dump("Player 2 wins");
        dump($dieRolls * $score[1]);
        $gameEnded = true;
    }
    $turn = ($turn == 1) ? 2 : 1;
}