<?php
/** @var string $input */
//$input = <<<INPUT
//Player 1 starting position: 4
//Player 2 starting position: 8
//INPUT;

$inputs = explode(PHP_EOL, $input);
$positions = [];
foreach ($inputs as $inputLine) {
    if (preg_match("/.*position: (?<pos>\d{1})/", $inputLine, $matches)) {
        $positions[] = (int)$matches['pos'];
    }
}

function move($start , $add)
{
    if (($start + $add) <= 10) {
        return $start + $add;
    }
    return (($start + $add) % 10 != 0) ? ($start + $add) % 10 : 10;
}

//$combinations = [];
//for ($a = 1; $a <= 3; $a++) {
//    for ($b = 1; $b <= 3; $b++) {
//        for ($c = 1; $c <= 3; $c++) {
//            $combinations[] = [$a,$b,$c];
//        }
//    }
//}
//
//$combinationSums = array_map('array_sum', $combinations);
//$combinationSumCount = array_count_values($combinationSums);

$p1pos = $positions[0];
$p2pos = $positions[1];
$p1score = 0;
$p2score = 0;

function getWins($p1pos, $p2pos, $p1score, $p2score) {
    $rolls = [3 => 1, 4 => 3, 5 => 6, 6 => 7, 7 => 6, 8 => 3, 9 => 1];
    $wins1 = 0;
    $wins2 = 0;
    foreach ($rolls as $roll => $count) {
        $player = move($p1pos, $roll);
        $score = $p1score + $player;
        if ($score >= 21) {
            $wins1 += $count;
        } else {
            [$wins2Sofar, $wins1Sofar] = getWins($p2pos, $player, $p2score, $score);
            $wins1 += $wins1Sofar * $count;
            $wins2 += $wins2Sofar * $count;
        }
    }
    return [$wins1, $wins2];
}

$totalWins = getWins($p1pos, $p2pos, 0, 0);
dd(max($totalWins));
