<?php
/** @var string $input */
$input = explode(chr(10).chr(10), trim($input));
$numbers = explode(',', array_shift($input));

$bingoCards = [];
foreach ($input as $bingoData) {
    $bingoCards[] = array_map(function ($row) {
        return array_values(explode(chr(32), str_replace('  ', ' ', $row)));
    }, explode(chr(10), $bingoData));
}

function testBingoCard($bingoCard, $numbers)
{
    foreach ($bingoCard as $rowKey => $row) {
        foreach ($row as $key => $number) {
            if (in_array($number, $numbers)) {
                $bingoCard[$rowKey][$key] = 'x';
            }
        }
    }


    $bingo = false;
    foreach ($bingoCard as $row) {
        $horizontalX = array_count_values($row);
        if (isset($horizontalX['x']) && $horizontalX['x'] == 5) {
            $bingo = true;
            break;
        }
    }

    if (!$bingo) {
        for ($c = 0; $c <= 4; $c++) {
            $columnCount = 0;
            foreach ($bingoCard as $row) {
                if ($row[$c] == 'x') {
                    $columnCount++;
                }
            }
            if ($columnCount == 5) {
                $bingo = true;
                break;
            }
        }
    }

    $sum = 0;
    foreach ($bingoCard as $row) {
        $sum += array_sum($row);
    }

    return [
        'card' => $bingoCard,
        'bingo' => $bingo,
        'sum' => (int)$sum
    ];
}

function callNumbers($numbers, $bingoCards)
{
    for ($i = 0; $i < count($numbers); $i++) {
        $numbersSoFar = array_slice($numbers, 0, $i);
        foreach ($bingoCards as $bingoCard) {
            $result = testBingoCard($bingoCard, $numbersSoFar);
            if ($result['bingo']) {
                $lastCalledNumber = (int)array_pop($numbersSoFar);
                dd($result['sum'] * $lastCalledNumber);
            }
        }
    }
}

callNumbers($numbers, $bingoCards);

