<?php
/** @var string $input */

$input = <<<DEBUG
..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###
DEBUG;

list($enhancement, $inputImageString) = explode(PHP_EOL.PHP_EOL, $input);
$enhancement = str_split($enhancement);
foreach (explode(chr(10), $inputImageString) as $line) {
    $inputImage[] = str_split($line);
}

$enhancedImage = enhance($inputImage, $enhancement);
paint($enhancedImage);
$enhancedImage = enhance($enhancedImage, $enhancement);
paint($enhancedImage);
dd(countLit($enhancedImage));
die;

function stretch(&$inputImage)
{
    foreach ($inputImage as &$stretchedImage) {
        array_unshift($stretchedImage, '.');
        array_unshift($stretchedImage, '.');
        array_push($stretchedImage, '.');
        array_push($stretchedImage, '.');
    }
    $rowCount = count($inputImage[0]);
    $emptyRow = array_fill(0, $rowCount, '.');
    array_unshift($inputImage, $emptyRow);
    array_unshift($inputImage, $emptyRow);
    array_push($inputImage, $emptyRow);
    array_push($inputImage, $emptyRow);
}

function countLit($inputImage)
{
    $countLit = 0;
    foreach ($inputImage as $y => $rows) {
        foreach ($rows as $x => $pixel) {
            if ($pixel == '#') {
                $countLit++;
            }
        }
    }
    return $countLit;
}

function enhance($inputImage, $enhancement)
{
    $outputImage = [];
    stretch($inputImage);

    foreach ($inputImage as $y => $rows) {
        foreach ($rows as $x => $pixel) {
            $squarePixel = [];
            for ($ySearch = 0; $ySearch <= 2; $ySearch++) {
                for ($xSearch = 0; $xSearch <= 2; $xSearch++) {
                    if (!isset($inputImage[$y+$ySearch][$x+$xSearch])) {
                        continue 3;
                    }
                    $squarePixel[] = $inputImage[$y+$ySearch][$x+$xSearch] == '#' ? '1' : '0';
                }
            }

            $outputImage[$y][$x] = $enhancement[bindec(implode($squarePixel))];
        }
    }
    return $outputImage;
}

function paint($picture)
{
    $rangeExtend = 5;
    $xRange = count($picture[0]);
    $yRange = count($picture);

    for ($x = 0 - $rangeExtend; $x < $xRange + $rangeExtend; $x++) {
        for ($y = 0 - $rangeExtend; $y < $yRange + $rangeExtend; $y++) {
            echo (!isset($picture[$x][$y]))
                ? '.'
                : $picture[$x][$y];
        }
        echo PHP_EOL;
    }
}