<?php
/** @var string $input */

//$input = <<<DEBUG
//..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#
//
//#..#.
//#....
//##..#
//..#..
//..###
//DEBUG;

list($enhancement, $inputImageString) = explode(PHP_EOL.PHP_EOL, $input);
$enhancement = str_split($enhancement);
foreach (explode(chr(10), $inputImageString) as $line) {
    $inputImage[] = str_split($line);
}

const UNLIT = 0;
const LIT = 1;
$state = UNLIT;

$enhanceTimes = 50;
for ($i = 1; $i <= $enhanceTimes; $i++) {
    stretch($inputImage, $state);
    $inputImage = enhance($inputImage, $enhancement, $state);
    $state = ($state == LIT) ? UNLIT : LIT;
}
dump(countLit($inputImage));
die;

function stretch(&$inputImage, $default)
{
    $defaultValues = [
        UNLIT => '.',
        LIT => '#',
    ];

    foreach ($inputImage as &$stretchedImage) {
        array_unshift($stretchedImage, $defaultValues[$default]);
        array_unshift($stretchedImage, $defaultValues[$default]);
        array_unshift($stretchedImage, $defaultValues[$default]);
        array_push($stretchedImage, $defaultValues[$default]);
        array_push($stretchedImage, $defaultValues[$default]);
        array_push($stretchedImage, $defaultValues[$default]);
    }
    $rowCount = count($inputImage[0]);
    $emptyRow = array_fill(0, $rowCount, $defaultValues[$default]);
    array_unshift($inputImage, $emptyRow);
    array_unshift($inputImage, $emptyRow);
    array_unshift($inputImage, $emptyRow);
    array_push($inputImage, $emptyRow);
    array_push($inputImage, $emptyRow);
    array_push($inputImage, $emptyRow);
}

function countLit($inputImage)
{
    $countLit = 0;
    foreach ($inputImage as $rows) {
        foreach ($rows as $pixel) {
            if ($pixel == '#') {
                $countLit++;
            }
        }
    }
    return $countLit;
}

function enhance($inputImage, $enhancement, $default)
{
    $outputImage = [];

    foreach ($inputImage as $y => $rows) {
        foreach ($rows as $x => $pixel) {
            $squarePixel = [];
            for ($ySearch = 0; $ySearch <= 2; $ySearch++) {
                for ($xSearch = 0; $xSearch <= 2; $xSearch++) {
                    if (!isset($inputImage[$y+$ySearch][$x+$xSearch])) {
                        $squarePixel[] = $default;
                    } else {
                        $squarePixel[] = $inputImage[$y+$ySearch][$x+$xSearch] == '#' ? '1' : '0';
                    }
                }
            }

            $outputImage[$y][$x] = $enhancement[bindec(implode($squarePixel))];
        }
    }
    return $outputImage;
}

function paint($picture, $default)
{
    $defaultValues = [
        UNLIT => '.',
        LIT => '#',
    ];

    $rangeExtend = 0;
    $xRange = count($picture[0]);
    $yRange = count($picture);

    for ($x = 0 - $rangeExtend; $x < $xRange + $rangeExtend; $x++) {
        for ($y = 0 - $rangeExtend; $y < $yRange + $rangeExtend; $y++) {
            echo (!isset($picture[$x][$y]))
                ? $defaultValues[$default]
                : $picture[$x][$y];
        }
        echo PHP_EOL;
    }
    echo PHP_EOL;
    echo PHP_EOL;
}
