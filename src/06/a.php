<?php
/** @var string $input */
$school = array_map('intval', explode(",", trim($input)));

for($i = 1; $i <= 80; $i++) {
    $newFish = [];
    foreach ($school as &$fish) {
        if ($fish == 0) {
            $fish = 6;
            $newFish[] = 8;
            continue;
        }
        $fish--;
    }
    $school = array_merge($school, $newFish);
}

dump(count($school));
