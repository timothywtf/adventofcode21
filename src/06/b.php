<?php
/** @var string $input */
//$input = file_get_contents(__DIR__ . '/../../input/debug/06.txt');
$school = array_map('intval', explode(",", trim($input)));
$school = array_count_values($school);
for ($i = 0; $i <= 8; $i++) {
    if (!isset($school[$i])) {
        $school[$i] = 0;
    }
}
krsort($school);
$days = 256;
while ($days > 0) {
    $zeros = $school[0];
    foreach ($school as $key => $item) {
        if ($key == 0) {
            break;
        }
        $school[$key-1] = $item;
    }
    $school[6] += $zeros;
    $school[8] = $zeros;
    $days--;
}

dd(array_sum($school));
