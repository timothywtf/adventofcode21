<?php
/** @var string $input */
$depths = explode(chr(10), trim($input));
$lastDepth = array_shift($depths);
$increase = 0;
while (!empty($depths)) {
    $latest = $lastDepth;
    $lastDepth = array_shift($depths);
    if ($lastDepth > $latest) {
        $increase++;
    }
}

dd($increase);
