<?php
/** @var string $input */
$depths = explode(chr(10), trim($input));
$increase = 0;
$lastDepth = null;

for ($i = 0; $i <= count($depths) - 3; $i++) {
    $currentWindow = array_sum([
        $depths[$i],
        $depths[$i+1],
        $depths[$i+2],
    ]);

    if ($lastDepth != null && $currentWindow > $lastDepth) {
        $increase++;
    }
    $lastDepth = $currentWindow;
}
dd($increase);
