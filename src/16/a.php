<?php
/** @var string $input */

function packetFactory() {
    return [
        'id' => uniqid(),
        'length' => 0,
        'version' => null,
        'type' => null,
        'lengthType' => null,
        'body' => ''
    ];
}

function readPacket($packet, &$stream, &$packets) {
    // read version
    $packet['version'] = bindec(implode('', array_splice($stream, 0, 3, [])));
    // read type ID
    $packet['type'] = bindec(implode('', array_splice($stream, 0, 3, [])));
    $packet['length'] += 6;
    // read body, either
    if ($packet['type'] == 4) {
        // read literal
        $lastPartFound = false;
        while (!$lastPartFound) {
            $part = array_splice($stream, 0, 5, []);
            $packet['length'] += 5;
            if (array_shift($part) == 0) {
                $lastPartFound = true;
            }
            $packet['body'] .= implode('', $part);
        }
    } else {
        // read operator, either
        $packet['lengthType'] = bindec(implode('', array_splice($stream, 0, 1, [])));
        $packet['length']++;
        if ($packet['lengthType'] == 0) {
            // read length
            $subPacketLength = bindec(implode('', array_splice($stream, 0, 15, [])));
            $packet['length'] += 15;
            $subStream = array_splice($stream, 0, $subPacketLength, []);
            while (!empty($subStream)) {
                $subPacket = packetFactory();
                $subPacket = readPacket($subPacket, $subStream, $packets);
                $packets[$subPacket['id']] = $subPacket;
            }
        } else {
            // read count
            $subPacketCount = bindec(implode('', array_splice($stream, 0, 11, [])));
            $packet['length'] += 11;
            $subPacketsCreated = 0;
            while ($subPacketsCreated < $subPacketCount) {
                $subPacket = packetFactory();
                $subPacket = readPacket($subPacket, $stream, $packets);
                $packets[$subPacket['id']] = $subPacket;
                $subPacketsCreated++;
            }
        }
    }

    return $packet;
}

function sumVersions($input) {
    $binInput = '';
    foreach (str_split($input) as $char) {
        $binInput .= sprintf("%04s", decbin(ord(hex2bin(sprintf("%02s", $char)))));
    }
    $stream = str_split($binInput);
    $packets = [];

    $rootPacket = packetFactory();
    $rootPacket = readPacket($rootPacket, $stream, $packets);
    $packets[$rootPacket['id']] = $rootPacket;

    $versionNumbers = array_map(function ($packet) {
        return $packet['version'];
    }, $packets);
    return array_sum($versionNumbers);
}

dump(sumVersions('8A004A801A8002F478')); // 16
dump(sumVersions('620080001611562C8802118E34')); // 12
dump(sumVersions('C0015000016115A2E0802F182340')); // 23
dump(sumVersions('A0016C880162017C3686B18A3D4780')); // 31
dump(sumVersions($input)); // ?