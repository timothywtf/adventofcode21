<?php
/** @var string $input */

function packetFactory() {
    return [
        'id' => uniqid(),
        'length' => 0,
        'version' => null,
        'type' => null,
        'lengthType' => null,
        'body' => ''
    ];
}

function readPacket($packet, &$stream, &$packets) {
    // read version
    $packet['version'] = bindec(implode('', array_splice($stream, 0, 3, [])));
    // read type ID
    $packet['type'] = bindec(implode('', array_splice($stream, 0, 3, [])));
    $packet['length'] += 6;
    // read body, either
    if ($packet['type'] == 4) {
        // read literal
        $lastPartFound = false;
        while (!$lastPartFound) {
            $part = array_splice($stream, 0, 5, []);
            $packet['length'] += 5;
            if (array_shift($part) == 0) {
                $lastPartFound = true;
            }
            $packet['body'] .= implode('', $part);
        }
        $packet['body'] = bindec($packet['body']);
    } else {
        // read operator, either
        $packet['lengthType'] = bindec(implode('', array_splice($stream, 0, 1, [])));
        $packet['length']++;
        $subPackets = [];
        if ($packet['lengthType'] == 0) {
            // read length
            $subPacketLength = bindec(implode('', array_splice($stream, 0, 15, [])));
            $packet['length'] += 15;
            $subStream = array_splice($stream, 0, $subPacketLength, []);

            while (!empty($subStream)) {
                $subPacket = packetFactory();
                $subPacket = readPacket($subPacket, $subStream, $packets);
                $packets[$subPacket['id']] = $subPacket;
                $subPackets[] = $subPacket;
            }
        } else {
            // read count
            $subPacketCount = bindec(implode('', array_splice($stream, 0, 11, [])));
            $packet['length'] += 11;

            while (count($subPackets) < $subPacketCount) {
                $subPacket = packetFactory();
                $subPacket = readPacket($subPacket, $stream, $packets);
                $packets[$subPacket['id']] = $subPacket;
                $subPackets[] = $subPacket;
            }
        }

        $subPacketValues = array_map(function ($subPacket) {
            return $subPacket['body'];
        }, $subPackets);

        switch ($packet['type']) {
            case 0:
                $packet['body'] = array_sum($subPacketValues);
            break;
            case 1:
                $packet['body'] = array_product($subPacketValues);
            break;
            case 2:
                $packet['body'] = min($subPacketValues);
            break;
            case 3:
                $packet['body'] = max($subPacketValues);
            break;
            case 5:
                $packet['body'] = ($subPacketValues[0] > $subPacketValues[1]) ? 1 : 0;
            break;
            case 6:
                $packet['body'] = ($subPacketValues[0] < $subPacketValues[1]) ? 1 : 0;
            break;
            case 7:
                $packet['body'] = ($subPacketValues[0] == $subPacketValues[1]) ? 1 : 0;
            break;
        }
    }

    return $packet;
}

function evaluate($input) {
    $binInput = '';
    foreach (str_split($input) as $char) {
        $binInput .= sprintf("%04s", decbin(ord(hex2bin(sprintf("%02s", $char)))));
    }
    $stream = str_split($binInput);
    $packets = [];

    $rootPacket = packetFactory();
    $rootPacket = readPacket($rootPacket, $stream, $packets);
    $packets[$rootPacket['id']] = $rootPacket;
    return $rootPacket['body'];
}

//dump(evaluate('C200B40A82')); // 3
//dump(evaluate('04005AC33890')); // 54
//dump(evaluate('880086C3E88112')); // 7
//dump(evaluate('CE00C43D881120')); // 9
//dump(evaluate('D8005AC2A8F0')); // 1
//dump(evaluate('F600BC2D8F')); // 0
//dump(evaluate('9C005AC2F8F0')); // 0
//dump(evaluate('9C0141080250320F1802104A08')); // 1
dump(evaluate($input)); // ?