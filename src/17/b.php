<?php
/** @var string $input */
//$input = <<<DEBUG
//target area: x=20..30, y=-10..-5
//DEBUG;
preg_match("/.*x=(?<x1>\-?\d*)\.*(?<x2>\-?\d*)\, y\=(?<y1>\-?\d*)\.*(?<y2>\-?\d*)/", $input, $matches);

function calcTrajectory($xVel, $yVel, $xRange1, $xRange2, $yRange1, $yRange2, &$hits)
{
    $highestY = null;
    $probe['x'] = 0;
    $probe['y'] = 0;
    while ($probe['x'] < $xRange2 && $probe['y'] > $yRange1) {
        $probe['x'] += $xVel;
        $probe['y'] += $yVel;
        $highestY = ($probe['y'] > $highestY || $highestY == null) ? $probe['y'] : $highestY;
        if ($xVel < 0) {
            $xVel++;
        } elseif ($xVel > 0)  {
            $xVel--;
        }
        $yVel--;
        if (
            $probe['x'] >= $xRange1
            && $probe['x'] <= $xRange2
            && $probe['y'] >= $yRange1
            && $probe['y'] <= $yRange2
        ) {
            $hits[] = $highestY;
            return;
        }
    }
}

$hits = [];
$xTryRange = 350;
$yTryRange = 350;
for ($xTry = $xTryRange*-1; $xTry < $xTryRange; $xTry++) {
    for ($yTry = $yTryRange*-1; $yTry < $yTryRange; $yTry++) {
        calcTrajectory($xTry,$yTry,$matches['x1'],$matches['x2'],$matches['y1'],$matches['y2'], $hits);
    }
}
dump(count($hits));
