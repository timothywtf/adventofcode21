<?php
/** @var string $input */
$input = explode(chr(10), $input);
/** @var array $input */
$easyDigits = 0;
foreach ($input as $row) {
    list($leftSide, $rightSide) = explode(' | ', $row);
    $leftSide = explode(' ', $leftSide);
    $rightSide = explode(' ', $rightSide);
    foreach ($rightSide as $rightSideItem) {
        if (in_array(strlen($rightSideItem), [7,4,2,3])) {
            $easyDigits++;
        }
    }
}
dd($easyDigits);