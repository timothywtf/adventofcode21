<?php
/** @var string $input */
$input = explode(chr(10), $input);
/** @var array $input */
$outputs = [];
foreach ($input as $row) {
    list($leftSide, $rightSide) = explode(' | ', $row);
    $leftSide = explode(' ', $leftSide);
    $rightSide = explode(' ', $rightSide);
    $extractDigit = function (&$digits, $find) {
        $trans = [1=>2, 4=>4, 7=>3, 8=>7];
        $count = $trans[$find];
        $foundDigits = array_filter($digits, function ($digit) use ($count) {
            return strlen($digit) == $count;
        });
        $foundDigit = reset($foundDigits);
        $key = array_search($foundDigit, $digits);
        unset($digits[$key]);
        return $foundDigit;
    };

    $oneDigit = $extractDigit($leftSide, 1);
    $twoDigit = null;
    $threeDigit = null;
    $fourDigit = $extractDigit($leftSide, 4);
    $fiveDigit = null;
    $sixDigit = null;
    $sevenDigit = $extractDigit($leftSide, 7);
    $eightDigit = $extractDigit($leftSide, 8);
    $nineDigit = null;
    $zeroDigit = null;

    $strDiff = function ($str1, $str2) {
        $diff = array_diff(str_split($str1), str_split($str2));
        if (count($diff) > 1) {
            return implode('', $diff);
        }
        return reset($diff);
    };
    $fiveLongs = array_filter($leftSide, function($digit) {return strlen($digit) == 5;});
    $sixLongs = array_filter($leftSide, function($digit) {return strlen($digit) == 6;});

    $c1 = $strDiff($sevenDigit, $oneDigit);

    $containsAll = function ($base, $mask){
        return empty(array_diff(str_split($mask), str_split($base)));
    };

    $c3l2 = $strDiff($eightDigit, $sevenDigit);
    $c3l2 = $strDiff($c3l2, $fourDigit);

    $onlyOneContains = function (&$collection, $contains, $containsAll) {
        $onlyOne = array_filter($collection, function ($collection) use ($containsAll, $contains) {
            return $containsAll($collection, $contains);
        });
        $onlyOne = reset($onlyOne);
        $key = array_search($onlyOne, $collection);
        unset($collection[$key]);
        return $onlyOne;
    };

    $twoDigit = $onlyOneContains($fiveLongs, $c3l2, $containsAll);
    $threeDigit = $onlyOneContains($fiveLongs, $sevenDigit, $containsAll);
    $fiveDigit = reset($fiveLongs);
    $nineDigit = $onlyOneContains($sixLongs, $fourDigit, $containsAll);
    $zeroDigit = $onlyOneContains($sixLongs, $sevenDigit, $containsAll);
    $sixDigit = reset($sixLongs);
    $translation = [
        $oneDigit => 1,
        $twoDigit => 2,
        $threeDigit => 3,
        $fourDigit => 4,
        $fiveDigit => 5,
        $sixDigit => 6,
        $sevenDigit => 7,
        $eightDigit => 8,
        $nineDigit => 9,
        $zeroDigit => 0,
    ];

    $output = '';
    foreach ($rightSide as $rightSideItem) {
        foreach ($translation as $translationKey => $value) {
            if (strlen($translationKey) == strlen($rightSideItem)) {
                if ($containsAll($translationKey, $rightSideItem)) {
                    $output .= $value;
                }
            }
        }
    }
    $outputs[] = (int)$output;
}

dd(array_sum($outputs));

/**
 *    c1
 * l1    r1
 *    c2
 * l2    r2
 *    c3
 *
 *
 * 1 4 7 8
 * verschil tussen 7 en 1 => c1
 * 1 4 7 8 / c1
 * verschil tussen 8 en 7 , 4 => c3 + l2
 * 1 4 7 8 / c1 (c3 + l2)
 * de enige 5-string inclusief (c3 + l2) => 2
 * 1 2 4 7 8 / c1 (c3 + l2)
 * de enige 5-string inclusief een 7 => 3
 * 1 2 3 4 7 8 / c1 (c3 + l2)
 * de enige overgebleven 5-string => 5
 * 1 2 3 4 5 7 8 / c1 (c3 + l2)
 * de enige 6-string inclusief een 4 => 9
 * 1 2 3 4 5 7 8 9 / c1 (c3 + l2)
 * de enige 6-string inclusief een 7 => 0
 * 1 2 3 4 5 7 8 9 0 / c1 (c3 + l2)
 * de enige overgebleven => 6
 * 1 2 3 4 5 6 7 8 9 0 / c1 (c3 + l2)
 */