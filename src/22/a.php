<?php
/** @var string $input */
$inputs = explode(chr(10), $input);
$bootInstructions = [];
foreach ($inputs as $input) {
    $bootInstruction = [];
    if (preg_match("/^(?<dir>on|off) x=(?<x1>\-?\d*)\.{2}(?<x2>\-?\d*)\,y=(?<y1>\-?\d*)\.{2}(?<y2>\-?\d*)\,z=(?<z1>\-?\d*)\.{2}(?<z2>\-?\d*)$/", $input, $matches)) {
        $bootInstruction['dir'] = $matches['dir'];
        $bootInstruction['x1'] = (int)$matches['x1'];
        $bootInstruction['x2'] = (int)$matches['x2'];
        $bootInstruction['y1'] = (int)$matches['y1'];
        $bootInstruction['y2'] = (int)$matches['y2'];
        $bootInstruction['z1'] = (int)$matches['z1'];
        $bootInstruction['z2'] = (int)$matches['z2'];
    }
    $bootInstructions[] = $bootInstruction;
}

$bootInstructions = array_filter($bootInstructions, function ($bootInstruction) {
    return !(
        $bootInstruction['x1'] < -50 ||
        $bootInstruction['x2'] > 50 ||
        $bootInstruction['y1'] < -50 ||
        $bootInstruction['y2'] > 50 ||
        $bootInstruction['z1'] < -50 ||
        $bootInstruction['z2'] > 50
    );
});

$cubes = [];
foreach ($bootInstructions as $bootInstruction) {
    for ($x = $bootInstruction['x1']; $x <= $bootInstruction['x2']; $x++) {
        for ($y = $bootInstruction['y1']; $y <= $bootInstruction['y2']; $y++) {
            for ($z = $bootInstruction['z1']; $z <= $bootInstruction['z2']; $z++) {
                $ident = $x . '_' . $y . '_' . $z;
                if ($bootInstruction['dir'] == 'on') {
                    $cubes[$ident] = $ident;
                } else {
                    if (isset($cubes[$ident])) {
                        unset($cubes[$ident]);
                    }
                }
            }
        }
    }
}
dd(count($cubes));