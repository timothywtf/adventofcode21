<?php
/** @var string $input */
//$input = <<<DEBUG
//6,10
//0,14
//9,10
//0,3
//10,4
//4,11
//6,0
//6,12
//4,1
//0,13
//10,12
//3,4
//3,0
//8,4
//1,10
//2,14
//8,10
//9,0
//
//fold along y=7
//fold along x=5
//DEBUG;

list($dots, $folds) = explode(chr(10).chr(10), $input);
$dots = explode(chr(10), $dots);
$folds = explode(chr(10), $folds);

$folds = array_map(function ($fold) {
    if (preg_match("/.*(?<axis>[x-y]{1})=(?<number>\d*)$/", $fold, $matches)) {
        return $matches;
    }
    return null;
}, $folds);

$coords = array_map(function ($dot) {
    return explode(',', $dot);
}, $dots);

$xRange = max(array_map(function ($coord) {return $coord[0];}, $coords));
$yRange = max(array_map(function ($coord) {return $coord[1];}, $coords));
//dump($dots);
//dump($coords);
//dump($folds);
//dump($xRange);
//dump($yRange);

//todo optimize
$grid = [];
for ($y = 0; $y <= $yRange; $y++) {
    for ($x = 0; $x <= $xRange; $x++) {
        $grid[$y][$x] = (in_array([$x, $y], $coords))
            ? '#'
            : '.';
    }
}

function paint($grid, $toFile = false) {
    $picture = '';
    foreach ($grid as $y => $xCoords) {
        foreach ($xCoords as $x => $pixel) {
            $picture .= $pixel;
        }
        $picture .= PHP_EOL;
    }
    if ($toFile) {
        file_put_contents(__DIR__ . '/dump13.txt', $picture);
    } else {
        echo $picture;
    }
}

function merge($left, $right)
{
    $merged = [];
    foreach ($left as $y => $xCoords) {
        foreach ($xCoords as $x => $pixel) {
            $mergedPixel = (
                $pixel == '#'
                || (isset($right[$y][$x]) && $right[$y][$x] == '#')
            )
                ? '#'
                : '.';
            $merged[$y][$x] = $mergedPixel;
        }
    }
    return $merged;
}

function sliceX($x, $grid) {
    $sliceLeft = [];
    $sliceRight = [];
    foreach ($grid as $y => $row) {
        $sliceLeft[] = array_slice($row, 0, $x);
        $sliceRight[] = array_slice($row, $x+1, count($row)-1);
    }
    return [$sliceLeft, $sliceRight];
}

function sliceY($y, $grid) {
    $slices[] = array_slice($grid, 0, $y);
    $slices[] = array_slice($grid, $y+1, count($grid)-1);
    return $slices;
}

function transformUp($grid)
{
    return array_reverse($grid);
}

function transformLeft($grid)
{
    $transformed = [];
    foreach ($grid as $y => $row) {
        $transformed[$y] = array_reverse($row);
    }
    return $transformed;
}

function foldLeft($x, $grid)
{
    $slices = sliceX($x, $grid);
    $slices[1] = transformLeft($slices[1]);
    return merge($slices[0], $slices[1]);
}

function foldUp($y, $grid)
{
    $slices = sliceY($y, $grid);
    $slices[1] = transformUp($slices[1]);
    return merge($slices[0], $slices[1]);
}

function countVisibleDots($grid)
{
    $count = 0;
    foreach ($grid as $y => $row) {
        $counts = array_count_values($row);
        $count += $counts['#'] ?? 0;
    }
    dd($count);
}

//paint($grid);
foreach ($folds as $fold) {
    $axis = $fold['axis'];
    $number = $fold['number'];
    if ($axis == 'y') {
        $grid = foldUp($number, $grid);
    } else if ($axis == 'x') {
        $grid = foldLeft($number, $grid);
    }
    countVisibleDots($grid);
}