<?php
/** @var string $input */
//$input = <<<DEBUG
//start-A
//start-b
//A-c
//A-b
//b-d
//A-end
//b-end
//DEBUG;
//$input = <<<DEBUG
//dc-end
//HN-start
//start-kj
//dc-start
//dc-HN
//LN-dc
//HN-end
//kj-sa
//kj-HN
//kj-dc
//DEBUG;
//$input = <<<DEBUG
//fs-end
//he-DX
//fs-he
//start-DX
//pj-DX
//end-zg
//zg-sl
//zg-pj
//pj-he
//RW-he
//fs-DX
//pj-RW
//zg-RW
//start-pj
//he-WI
//zg-he
//pj-fs
//start-RW
//DEBUG;


$paths = explode(chr(10), $input);
$uniqueNodes = [];
$nodes = [];
foreach ($paths as $path) {
    $currentNode = explode('-', $path);
    $nodes = array_merge($nodes, $currentNode);
    $uniqueNodes[$currentNode[0]] = $currentNode[0];
    $uniqueNodes[$currentNode[1]] = $currentNode[1];
}

$graph = new \Fhaculty\Graph\Graph();
$vertices = [];
foreach ($uniqueNodes as $uniqueNode) {
    $vertices[$uniqueNode] = $graph->createVertex($uniqueNode);
}

foreach ($paths as $path) {
    if (preg_match("/(?<left>.*)\-(?<right>.*)/", $path, $matches)) {
        $leftVertex = $vertices[$matches['left']];
        $rightVertex = $vertices[$matches['right']];
        $leftVertex->createEdge($rightVertex);
    }
}

$queue = [[$vertices['start']]];
$paths = [];

while (!empty($queue)) {
    /** @var \Fhaculty\Graph\Vertex $currentNode */
    $currentState = array_shift($queue);
    $lastNode = end($currentState);
    reset($currentState);
    foreach ($lastNode->getVerticesEdgeFrom() as $item) {
        if ($item->getId() == 'start') {
            continue;
        }
        $stateCandidate = $currentState;
        $stateCandidate[] = $item;

        if (ctype_lower($item->getId())) {
            $visitedLowerCaseNodes = array_map(function ($node) {
                return $node->getId();
            }, $stateCandidate);
            $visitedCount = array_filter(array_count_values($visitedLowerCaseNodes), function ($count) {
                return ctype_lower($count) && $count != 'end' && $count != 'start';
            }, ARRAY_FILTER_USE_KEY);
            $doubles = array_filter($visitedCount, function ($occurrence) {
                return $occurrence == 2;
            });

            if (in_array(3, $visitedCount) || count($doubles) > 1) {
                continue;
            }
        }

        $newState = $stateCandidate;
        $visitedLowerCaseNodesTest = array_map(function ($node) {
            return $node->getId();
        }, $newState);
        if ($item->getId() == 'end') {
            $paths[] = $newState;
            $implode = implode(', ', array_map(function ($vertex) {
                return $vertex->getId();
            }, $newState));
            $readablePaths[] = $implode;
        } else {
            $queue[] = $newState;
        }
    }
}


//dump($readablePaths);
dd(count($readablePaths));