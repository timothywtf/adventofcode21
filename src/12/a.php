<?php
/** @var string $input */
$paths = explode(chr(10), $input);
$uniqueNodes = [];
$nodes = [];
foreach ($paths as $path) {
    $currentNode = explode('-', $path);
    $nodes = array_merge($nodes, $currentNode);
    $uniqueNodes[$currentNode[0]] = $currentNode[0];
    $uniqueNodes[$currentNode[1]] = $currentNode[1];
}

$countNodes = array_count_values($nodes);
$lowerCaseLeafNodes = array_filter($countNodes, function ($countNode) {
    return $countNode == 1;
});

foreach ($lowerCaseLeafNodes as $lowerCaseLeafNode => $count) {
    foreach ($paths as $pathKey => $path) {
        if (preg_match("/(?<left>[a-z]*)\-(?<right>[a-z]*)/", $path, $matches)) {
            if ($lowerCaseLeafNode == $matches['left']) {
                if (ctype_lower($matches['right'])) {
                    unset($paths[$pathKey]);
                    unset($uniqueNodes[$matches['left']]);
                }
            } elseif ($lowerCaseLeafNode == $matches['right']) {
                if (ctype_lower($matches['left'])) {
                    unset($paths[$pathKey]);
                    unset($uniqueNodes[$matches['right']]);
                }
            }
        }
    }
}

$graph = new \Fhaculty\Graph\Graph();
$vertices = [];
foreach ($uniqueNodes as $uniqueNode) {
    $vertices[$uniqueNode] = $graph->createVertex($uniqueNode);
}

foreach ($paths as $path) {
    if (preg_match("/(?<left>.*)\-(?<right>.*)/", $path, $matches)) {
        $leftVertex = $vertices[$matches['left']];
        $rightVertex = $vertices[$matches['right']];
        $leftVertex->createEdge($rightVertex);
    }
}

$queue = [[$vertices['start']]];
$paths = [];

while (!empty($queue)) {
    /** @var \Fhaculty\Graph\Vertex $currentNode */
    $currentState = array_shift($queue);
    $lastNode = end($currentState);
    reset($currentState);
    foreach ($lastNode->getVerticesEdgeFrom() as $item) {
        if ($item->getId() == 'start') {
            continue;
        }
        $newState = $currentState;

        if (ctype_lower($item->getId())) {
            $visitedLowerCaseNodes = array_map(function ($node) {
                return $node->getId();
            }, $newState);
            if (in_array($item->getId(), $visitedLowerCaseNodes)) {
                continue;
            }
        }
        $newState[] = $item;
        $visitedLowerCaseNodesTest = array_map(function ($node) {
            return $node->getId();
        }, $newState);
        if ($item->getId() == 'end') {
            $paths[] = $newState;
            $implode = implode(', ', array_map(function ($vertex) {
                return $vertex->getId();
            }, $newState));
            $readablePaths[] = $implode;
        } else {
            $queue[] = $newState;
        }
    }
}

dd(count($readablePaths));