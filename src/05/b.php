<?php
/** @var string $input */
$lines = array_map(function ($inputLine){
    preg_match("/(?<x1>\d*),(?<y1>\d*) \-> (?<x2>\d*),(?<y2>\d*)/", $inputLine, $matches);
    return array_map('intval', array_filter($matches, function ($key) {return is_string($key);}, ARRAY_FILTER_USE_KEY));
}, explode(chr(10), trim($input)));

$nonDiagonalLines = array_filter($lines, function ($line) {
    return ($line['x1'] == $line['x2'] || $line['y1'] == $line['y2']);
});
$diagonalLines = array_filter($lines, function ($line) {
    return ($line['x1'] != $line['x2'] && $line['y1'] != $line['y2']);
});

$coords = [];
foreach ($nonDiagonalLines as $nonDiagonalLine) {
    if ($nonDiagonalLine['x1'] == $nonDiagonalLine['x2']) {
        $vector = [$nonDiagonalLine['y1'], $nonDiagonalLine['y2']];
        natsort($vector);
        $vector = array_values($vector);
        for ($i = $vector[0]; $i <= $vector[1]; $i++) {
            $coordKey = $nonDiagonalLine['x1'] . ',' . $i;
            $coords[$coordKey] = (!isset($coords[$coordKey]))
                ? 1
                : $coords[$coordKey] + 1;
        }
    } elseif ($nonDiagonalLine['y1'] == $nonDiagonalLine['y2']) {
        $vector = [$nonDiagonalLine['x1'], $nonDiagonalLine['x2']];
        natsort($vector);
        $vector = array_values($vector);
        for ($i = $vector[0]; $i <= $vector[1]; $i++) {
            $coordKey = $i . ',' . $nonDiagonalLine['y1'];
            $coords[$coordKey] = (!isset($coords[$coordKey]))
                ? 1
                : $coords[$coordKey] += 1;
        }
    } else {
        die("somethings wrong");
    }
}

foreach ($diagonalLines as $diagonalLine) {
    $yVector = [$diagonalLine['y1'], $diagonalLine['y2']];
    $delta = abs($yVector[0] - $yVector[1]);
    $yDirectionPositive = $diagonalLine['y1'] < $diagonalLine['y2'];

    $xVector = [$diagonalLine['x1'], $diagonalLine['x2']];
    $xDirectionPositive = $diagonalLine['x1'] < $diagonalLine['x2'];

    for ($j = 0; $j <= $delta; $j++) {
        $nextXCoord = ($xDirectionPositive) ? $xVector[0] + $j : $xVector[0] - $j;
        $nextYCoord = ($yDirectionPositive) ? $yVector[0] + $j : $yVector[0] - $j;
        $coordKey = $nextXCoord . ',' . $nextYCoord;
        $coords[$coordKey] = (!isset($coords[$coordKey]))
            ? 1
            : $coords[$coordKey] += 1;
    }
}

$overlapPoints = array_count_values($coords);
dump(array_sum(array_filter($overlapPoints, function ($count) {return $count > 1;}, ARRAY_FILTER_USE_KEY)));
