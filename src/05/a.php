<?php
/** @var string $input */

//$input = file_get_contents(__DIR__ . '/../../input/debug/05.txt');

$lines = array_map(function ($inputLine){
    preg_match("/(?<x1>\d*),(?<y1>\d*) \-> (?<x2>\d*),(?<y2>\d*)/", $inputLine, $matches);
    return array_filter($matches, function ($key) {return is_string($key);}, ARRAY_FILTER_USE_KEY);
}, explode(chr(10), trim($input)));

$nonDiagonalLines = array_filter($lines, function ($line) {
    return ($line['x1'] == $line['x2'] || $line['y1'] == $line['y2']);
});

$coords = [];

foreach ($nonDiagonalLines as $nonDiagonalLine) {
    if ($nonDiagonalLine['x1'] == $nonDiagonalLine['x2']) {
        $vector = [$nonDiagonalLine['y1'], $nonDiagonalLine['y2']];
        natsort($vector);
        $vector = array_values($vector);
        for ($i = $vector[0]; $i <= $vector[1]; $i++) {
            $coordKey = $nonDiagonalLine['x1'] . ',' . $i;
            $coords[$coordKey] = (!isset($coords[$coordKey]))
                ? 1
                : $coords[$coordKey] + 1;
        }
    } elseif ($nonDiagonalLine['y1'] == $nonDiagonalLine['y2']) {
        $vector = [$nonDiagonalLine['x1'], $nonDiagonalLine['x2']];
        natsort($vector);
        $vector = array_values($vector);
        for ($i = $vector[0]; $i <= $vector[1]; $i++) {
            $coordKey = $i . ',' . $nonDiagonalLine['y1'];
            $coords[$coordKey] = (!isset($coords[$coordKey]))
                ? 1
                : $coords[$coordKey] += 1;
        }
    } else {
        die("somethings wrong");
    }
}

//$limit = 9;
//for ($y = 0; $y <= $limit; $y++) {
//
//    for ($x = 0; $x <= $limit; $x++) {
//        if (isset($coords[$x . ',' . $y])) {
//            echo $coords[$x . ',' . $y];
//        } else {
//            echo ".";
//        }
//    }
//    echo chr(10);
//}
//
//dd($coords);

$overlapPoints = array_count_values($coords);
dump(array_sum(array_filter($overlapPoints, function ($count) {return $count > 1;}, ARRAY_FILTER_USE_KEY)));
