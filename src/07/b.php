<?php
/** @var string $input */
$positions = array_map('intval', explode(",", trim($input)));

$avg = round(array_sum($positions)/count($positions));

$fuel = [];
for ($i = 0; $i <= count($positions); $i++) {
    foreach ($positions as $position) {
        $range = [$position, $i];
        sort($range);
        $fuel[$i][] = array_sum(range(1, ($range[1] - $range[0])));
    }
}

dd(min(array_map('array_sum', $fuel)));
