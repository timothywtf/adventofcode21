<?php
/** @var string $input */
$moves = array_map(function ($row) {
    preg_match("/(?<direction>[a-z]*) (?<units>\d+)/", $row, $matches);
    return [
        'direction' => $matches['direction'],
        'units' => (int)$matches['units'],
    ];
}, explode(chr(10), trim($input)));

$x = 0;
$y = 0;
$aim = 0;

while (!empty($moves)) {
    $move = array_shift($moves);
    switch ($move['direction']) {
        case 'forward':
            $x += $move['units'];
            $y += $aim * $move['units'];
            break;
        case 'up': $aim -= $move['units'];
            break;
        case 'down': $aim += $move['units'];
            break;
    }
}

dd($x*$y);
