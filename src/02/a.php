<?php
/** @var string $input */
$moves = array_map(function ($row) {
    preg_match("/(?<direction>[a-z]*) (?<units>\d+)/", $row, $matches);
    return [
        'direction' => $matches['direction'],
        'units' => $matches['units'],
    ];
}, explode(chr(10), trim($input)));

$x = 0;
$y = 0;

while (!empty($moves)) {
    $move = array_shift($moves);
    switch ($move['direction']) {
        case 'forward': $x += $move['units'];
        break;
        case 'up': $y -= $move['units'];
        break;
        case 'down': $y += $move['units'];
        break;
    }
}

dd($x*$y);
