<?php
/** @var string $input */
$input = <<<DEBUG
NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C
DEBUG;


list($template, $insertions) = explode(chr(10).chr(10), $input);
$insertions = explode(chr(10), $insertions);
$insertions = array_map(function ($insertion) {
    return explode(' -> ', $insertion);
}, $insertions);

$queue = [];

$range = 10;
for ($i = 0; $i < $range; $i++) {
    foreach ($insertions as $insertion) {
        $lastPos = 0;
        $positions = array();
        while (($lastPos = strpos($template, $insertion[0], $lastPos)) !== false) {
            $positions[] = $lastPos;
            $lastPos++;
        }

        if (!empty($positions)) {
            foreach ($positions as $position) {
                $queue[$position+1] = $insertion[1];
            }
        }
    }
    ksort($queue);
    $offset = 0;
    foreach ($queue as $key => $queueItem) {
        $replacement = $queueItem . substr($template, $key+$offset, 1);
        $template = substr_replace($template, $replacement, $key+$offset, 1);
        $offset++;
    }
}

$counts = array_count_values(str_split($template));
sort($counts);
dump($counts);
dd(max($counts) - min($counts));