<?php
/** @var string $input */
list($template, $insertions) = explode(chr(10).chr(10), $input);
$insertions = explode(chr(10), $insertions);
$insertions = array_map(function ($insertion) {
    return explode(' -> ', $insertion);
}, $insertions);

$letters = array_count_values(str_split($template));
$pairs = [];
for ($i = 0; $i < strlen($template) - 1; $i++) {
    if (!isset($pairs[substr($template, $i, 2)])) {
        $pairs[substr($template, $i, 2)] = 1;
    } else {
        $pairs[substr($template, $i, 2)]++;
    }
}

$range = 40;
for ($i = 0; $i < $range; $i++) {
    $toAdd = [];

    $toRemove = [];
    $lettersToAdd = [];
    foreach ($insertions as $insertion) {
        $oldPair = $insertion[0];
        $splitPair = str_split($oldPair);
        $newLetter = $insertion[1];
        $newPair = [];
        $newPair[] = $splitPair[0] . $newLetter;
        $newPair[] = $newLetter . $splitPair[1];

        if (!isset($pairs[$oldPair]) || ($pairs[$oldPair] === 0)) {
            continue;
        }
        $currentPairCount = $pairs[$oldPair];

        if (!isset($toRemove[$oldPair])) {
            $toRemove[$oldPair] = 0;
        }
        $toRemove[$oldPair]++;

        if (!isset($lettersToAdd[$newLetter])) {
            $lettersToAdd[$newLetter] = $currentPairCount;
        } else {
            $lettersToAdd[$newLetter] += $currentPairCount;
        }

        if (!isset($toAdd[$newPair[0]]) || $toAdd[$newPair[0]] === 0) {
            $toAdd[$newPair[0]] = $currentPairCount;
        } else {
            $toAdd[$newPair[0]] += $currentPairCount;
        }

        if (!isset($toAdd[$newPair[1]]) || $toAdd[$newPair[1]] === 0) {
            $toAdd[$newPair[1]] = $currentPairCount;
        } else {
            $toAdd[$newPair[1]] += $currentPairCount;
        }
    }

    foreach ($toRemove as $itemToRemove => $countToRemove) {
        if (isset($pairs[$countToRemove])) {
            $pairs[$itemToRemove] -= $countToRemove;
        } else {
            $pairs[$itemToRemove] = 0;
        }
    }

    foreach ($toAdd as $itemToAdd => $countToAdd) {
        if (isset($pairs[$itemToAdd])) {
            $pairs[$itemToAdd] += $countToAdd;
        } else {
            $pairs[$itemToAdd] = $countToAdd;
        }
    }

    foreach ($lettersToAdd as $letterToAdd => $countToAdd) {
        if (isset($letters[$letterToAdd])) {
            $letters[$letterToAdd] += $countToAdd;
        } else {
            $letters[$letterToAdd] = $countToAdd;
        }
    }
}

sort($letters);
dd(max($letters) - min($letters));