<?php
namespace AOC\Path;

abstract class Cave implements \JMGQ\AStar\DomainLogicInterface
{

    /**
     * @param Node $node
     * @return iterable
     */
    public function getAdjacentNodes(mixed $node): iterable
    {
        return $node->getAdjacentNodes();
    }

    /**
     * @param Node $node
     * @param Node $adjacent
     * @return float|int
     */
    public function calculateRealCost(mixed $node, mixed $adjacent): float|int
    {
        return $adjacent->getRisk();
    }

    /**
     * @param Node $fromNode
     * @param Node $toNode
     * @return float|int
     */
    public abstract function calculateEstimatedCost(mixed $fromNode, mixed $toNode): float|int;
}