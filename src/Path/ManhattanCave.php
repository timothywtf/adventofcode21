<?php

namespace AOC\Path;

class ManhattanCave extends Cave
{
    /**
     * @param Node $fromNode
     * @param Node $toNode
     * @return float|int
     */
    public function calculateEstimatedCost(mixed $fromNode, mixed $toNode): float|int
    {
        return abs($fromNode->getX() - $toNode->getX()) + abs ($fromNode->getY() - $toNode->getY());
    }
}