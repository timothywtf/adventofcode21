<?php

namespace AOC\Path;

class DiagonalCave extends Cave
{
    /**
     * @param Node $fromNode
     * @param Node $toNode
     * @return float|int
     */
    public function calculateEstimatedCost(mixed $fromNode, mixed $toNode): float|int
    {
        /**
            dx = abs(current_cell.x – goal.x)
            dy = abs(current_cell.y – goal.y)
            h = D * (dx + dy) + (D2 - 2 * D) * min(dx, dy)
         */
        $dx = abs($fromNode->getX() - $toNode->getX());
        $dy = abs($fromNode->getY() - $toNode->getY());
        $h = 1 * ($dx + $dy) + (sqrt(2) - 2 * 1) * min($dx, $dy);
        return $h;
    }

}