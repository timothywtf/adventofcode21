<?php
namespace AOC\Path;

/**
 *
 */
class Node implements \JMGQ\AStar\Node\NodeIdentifierInterface
{
    /**
     * @var
     */
    protected $x;
    /**
     * @var
     */
    protected $y;

    /**
     * @var
     */
    protected $risk;

    /**
     * @var array
     */
    protected $adjacentNodes = [];

    /**
     * @param $x
     * @param $y
     * @param $risk
     */
    public function __construct($x, $y, $risk)
    {
        $this->x = $x;
        $this->y = $y;
        $this->risk = $risk;
    }

    /**
     * @return string
     */
    public function getUniqueNodeId(): string
    {
        return $this->x . '-' . $this->y;
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param mixed $x
     */
    public function setX($x): void
    {
        $this->x = $x;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param mixed $y
     */
    public function setY($y): void
    {
        $this->y = $y;
    }

    /**
     * @return mixed
     */
    public function getRisk()
    {
        return $this->risk;
    }

    /**
     * @param mixed $risk
     */
    public function setRisk($risk): void
    {
        $this->risk = $risk;
    }

    /**
     * @return array
     */
    public function getAdjacentNodes(): array
    {
        return $this->adjacentNodes;
    }

    /**
     * @param array $adjacentNodes
     */
    public function setAdjacentNodes(array $adjacentNodes): void
    {
        $this->adjacentNodes = $adjacentNodes;
    }

    /**
     * @param Node $node
     * @return void
     */
    public function addAdjacentNode(Node $node): void
    {
        $this->adjacentNodes[$node->getUniqueNodeId()] = $node;
    }
}