<?php

namespace AOC\Path;

class EuclideanCave extends Cave
{
    /**
     * @param Node $fromNode
     * @param Node $toNode
     * @return float|int
     */
    public function calculateEstimatedCost(mixed $fromNode, mixed $toNode): float|int
    {

        return sqrt((pow(($fromNode->getX() - $toNode->getX()), 2)) + (pow(($fromNode->getY() - $toNode->getY()), 2)));
    }

}