<?php
/** @var string $input */

$inputs = explode(chr(10), $input);
$largest = null;
for ($i = 0; $i < count($inputs); $i++) {
    for ($j = 0; $j < count($inputs); $j++) {
        if ($i == $j) {
            continue;
        }
        $response = addNumbers($inputs[$i], $inputs[$j]);
        $largest = ($response['magnitude'] > $largest || $largest == null)
            ? $response['magnitude']
            : $largest;
    }
}
dd($largest);

function addNumbers($number1, $number2)
{
    $orderRegister = [];
    $combinedNumber = combine($number1, $number2);
    $parsedNumber = parse($combinedNumber, $orderRegister);
    $reducedNumber = reduce($parsedNumber, $orderRegister);
    $magnitude = computeMagnitude($reducedNumber);
    return ['magnitude' => $magnitude, 'result' => simplify($reducedNumber)];
}

function combine($number1, $number2)
{
    return sprintf("[%s,%s]", $number1, $number2);
}

function parse($number, &$orderRegister)
{
    $parsedNumber = [];
    $elements = str_split($number);
    $currentId = null;
    while (!empty($elements)) {
        $char = array_shift($elements);
        if ($char == '[') {
            $newElement = [
                'id' => uniqid(),
                'level' => (isset($parsedNumber[$currentId]['level'])) ? $parsedNumber[$currentId]['level'] + 1 : 1,
                'assign' => 'left',
                'left' => null,
                'right' => null,
                'parent' => $parsedNumber[$currentId]['id'] ?? null,
                'child_position' => $parsedNumber[$currentId]['assign'] ?? null
            ];
            $parsedNumber[$newElement['id']] = $newElement;
            $currentId = $newElement['id'];
        } elseif ($char == ']') {
            $currentId = $parsedNumber[$currentId]['parent'];
        } elseif ($char == ',') {
            $parsedNumber[$currentId]['assign'] = 'right';
        } else {
            $currentElement = $parsedNumber[$currentId];
            $digit = (int)$char;
            $parsedNumber[$currentId][$currentElement['assign']] = $digit;
            upsert($parsedNumber[$currentId], $digit, $currentElement['assign'], $orderRegister);
        }
    }
    return $parsedNumber;
}

function reduce($number, &$orderRegister) {
    $reducedNumber = $number;
    $somethingToDo = true;
    while ($somethingToDo) {
        $toExplode = findToExplode($reducedNumber);
        if (!is_null($toExplode)) {
            $reducedNumber = explodePair($reducedNumber, $toExplode, $orderRegister);
            continue;
        }
        $toSplit = findToSplit($reducedNumber, $orderRegister);
        if (!is_null($toSplit)) {
            $reducedNumber = splitPair($reducedNumber, $toSplit, $orderRegister);
            continue;
        }
        $somethingToDo = !is_null($toExplode) && !is_null($toSplit);
    }
    return $reducedNumber;
}

function simplify($number)
{
    $allNumberCount = count($number);
    $completeNumbers = [];
    while (count($completeNumbers) !== $allNumberCount) {
        $completeNumbers = array_filter($number, function ($element) {
            return is_scalar($element['left']) && is_scalar($element['right']);
        });
        foreach ($completeNumbers as $completeNumber) {
            if (is_null($completeNumber['parent'])) {
                continue;
            }
            $parent = $number[$completeNumber['parent']];
            $parentPosition = $completeNumber['child_position'];
            $parent[$parentPosition] = sprintf("[%s,%s]", $completeNumber['left'], $completeNumber['right']);
            $number[$parent['id']] = $parent;
        }
    }
    $level1Element = array_filter($number, function ($numberElement) {
        return $numberElement['level'] === 1;
    });
    $level1Element = array_pop($level1Element);
    return sprintf("[%s,%s]", $level1Element['left'], $level1Element['right']);
}

function computeMagnitude($number)
{
    $magnitudeCalc = $number;
    $allNumberCount = count($magnitudeCalc);
    $completeNumbers = [];

    $magnitude = function ($left, $right) {
        return (3 * $left) + (2 * $right);
    };
    while (count($completeNumbers) !== $allNumberCount) {
        $completeNumbers = array_filter($magnitudeCalc, function ($element) {
            return is_int($element['left']) && is_int($element['right']);
        });
        foreach ($completeNumbers as $completeNumber) {
            if (is_null($completeNumber['parent'])) {
                continue;
            }
            $parent = $magnitudeCalc[$completeNumber['parent']];
            $parentPosition = $completeNumber['child_position'];
            $leftPart = $completeNumber['left'];
            $rightPart = $completeNumber['right'];
            $parent[$parentPosition] = $magnitude($leftPart, $rightPart);
            $magnitudeCalc[$parent['id']] = $parent;
        }
    }
    $level1Element = array_filter($magnitudeCalc, function ($numberElement) {
        return $numberElement['level'] === 1;
    });
    $level1Element = array_pop($level1Element);
    return $magnitude($level1Element['left'], $level1Element['right']);
}

function upsert($pair, $digit, $position, &$orderRegister) {
    $orderRegister[$pair['id'] . '-' . $position] = $digit;
}

function remove($keys, &$orderRegister) {
    foreach ($keys as $key) {
        if (isset($orderRegister[$key])) {
            unset($orderRegister[$key]);
        }
    }
}

function getNeighbors($pair, &$orderRegister) {
    $leftSide = [];
    $rightSide = [];
    $found = false;
    foreach ($orderRegister as $key => $item) {
        if (stristr($key, $pair['id'])) {
            $found = true;
            continue;
        }
        if (!$found) {
            $leftSide[] = $key;
        } else {
            $rightSide[] = $key;
        }
    }
    return [
        'left' => array_pop($leftSide) ?? null,
        'right' => array_shift($rightSide) ?? null
    ];
}

function addAfter($pairKey, $pairValue, &$orderRegister, $afterKey = null)
{
    $newOrderRegister = [];
    foreach ($orderRegister as $key => $item) {
        if (!isset($newOrderRegister[$key])) {
            $newOrderRegister[$key] = $item;
        }
        if ($key == $afterKey) {
            $newOrderRegister[$pairKey] = $pairValue;
        }
    }
    if ($afterKey == null) {
        $newOrderRegister[$pairKey] = $pairValue;
    }
    $orderRegister = $newOrderRegister;
}

function addBefore($pairKey, $pairValue, &$orderRegister, $beforeKey = null)
{
    $newOrderRegister = [];
    if ($beforeKey == null) {
        $newOrderRegister[$pairKey] = $pairValue;
    }
    foreach ($orderRegister as $key => $item) {
        if ($key == $beforeKey) {
            $newOrderRegister[$pairKey] = $pairValue;
        }
        if (!isset($newOrderRegister[$key])) {
            $newOrderRegister[$key] = $item;
        }
    }
    $orderRegister = $newOrderRegister;
}

function findToExplode($number)
{
    foreach ($number as $pair) {
        if (is_int($pair['left']) && is_int($pair['right']) && ($pair['level'] - 4 > 0)) {
            return $pair;
        }
    }
    return null;
}

function explodePair($number, $pair, &$orderRegister)
{
    $parent = $number[$pair['parent']];
    $shouldAddToLeft = $parent['left'] === null;
    $shouldAddToRight = $parent['right'] === null;

    $neighbors = getNeighbors($pair, $orderRegister);

    $toDistributeToLeft = $pair['left'];
    $toDistributeToRight = $pair['right'];

    unset($number[$pair['id']]);

    if ($shouldAddToLeft) {
        $parent['left'] = 0;
        $number[$parent['id']] = $parent;
        addBefore($parent['id'] . '-left', 0, $orderRegister, $pair['id'] . '-left');
        remove([$pair['id'] . '-right', $pair['id'] . '-left'], $orderRegister);
    } elseif ($shouldAddToRight) {
        $parent['right'] = 0;
        $number[$parent['id']] = $parent;
        addAfter($parent['id'] . '-right', 0, $orderRegister, $pair['id'] . '-right');
        remove([$pair['id'] . '-right', $pair['id'] . '-left'], $orderRegister);
    }

    //left
    if (!is_null($neighbors['left'])) {
        $leftNeighborKey = $neighbors['left'];
        $explodeParts = explode('-', $leftNeighborKey);
        $leftNeighborKey = $explodeParts[0];
        $leftNeighborPosition = $explodeParts[1];
        $leftNeighbor = $number[$leftNeighborKey];

        // is leftSide $shouldAddToLeft
        if ($shouldAddToLeft) {
            // set reference = leftneighbor
            $reference = $leftNeighborKey;
            // calc new number $toDistributeToLeft + left neighbor
            $newLeftValue = $toDistributeToLeft + $leftNeighbor[$leftNeighborPosition];
            // update neigbor in numbers
            $number[$leftNeighborKey][$leftNeighborPosition] = $newLeftValue;
            // update neighbor in orderref
            //checked
            upsert($leftNeighbor, $newLeftValue, $leftNeighborPosition, $orderRegister);
        } elseif ($shouldAddToRight) { // is rightSide $shouldAddToRight
            // calc new number $toDistributeToLeft + parent left
            $newLeftValue = $toDistributeToLeft + $parent['left'];
            // update parent left in numbers
            $parent['left'] = $newLeftValue;
            $number[$parent['id']] = $parent;
            // update parent left in orderref
            upsert($parent, $newLeftValue, 'left', $orderRegister);
        }
    } else { // geen left nieghbor
        if ($shouldAddToLeft) {// is leftSide

        } elseif ($shouldAddToRight) {// is rightSide
            // set reference = parent left
            // calc new number pair left + parent left
            $newLeftValue = $parent['left'] + $toDistributeToLeft;
            // update parent left in numbers
            $parent['left'] = $newLeftValue;
            $number[$parent['id']] = $parent;
            // update parent left in orderref
            upsert($parent, $newLeftValue, 'left', $orderRegister);
        }

    }


    //right
    if (!is_null($neighbors['right'])) {
        $rightNeighborKey = $neighbors['right'];
        $explodeParts = explode('-', $rightNeighborKey);
        $rightNeighborKey = $explodeParts[0];
        $rightNeighborPosition = $explodeParts[1];
        $rightNeighbor = $number[$rightNeighborKey];

        // is right side
        if ($shouldAddToRight) {
            // set reference = rightneighbor
            // calc new number $toDistributeToRight + right neighbor
            $newRightValue = $toDistributeToRight + $rightNeighbor[$rightNeighborPosition];
            // update neigbor in numbers
            $number[$rightNeighborKey][$rightNeighborPosition] = $newRightValue;
            // update neighbor in orderref
            upsert($rightNeighbor, $newRightValue, $rightNeighborPosition, $orderRegister);

        } elseif ($shouldAddToLeft) { // is leftside
            // set reference = parent right
            // calc new number $toDistributeToRight + parent right
            $newRightValue = $toDistributeToRight + $parent['right'];
            // update parent right in numbers
            $parent['right'] = $newRightValue;
            $number[$parent['id']] = $parent;
            // update parent right in orderref
            //checked
            upsert($parent, $newRightValue, 'right', $orderRegister);
        }

    } else {
        // is right side
        if ($shouldAddToRight) {
            // geen reference just prepend
            // set parent right = 0 in numbers
        } elseif ($shouldAddToLeft) { // is leftside
            // set reference = parent right
            // calc new number todistright + parent right
            $newRightValue = $parent['right'] + $toDistributeToRight;
            // update parent right in numbers
            $parent['right'] = $newRightValue;
            $number[$parent['id']] = $parent;
            // update parent right in orderref
            upsert($parent, $newRightValue, 'right', $orderRegister);
        }

    }

    return $number;
}

function findToSplit($number, &$orderRegister)
{
    $pairsToSplit = array_filter($orderRegister, function ($digit) {
        return $digit >= 10;
    });
    if (!empty($pairsToSplit)) {
        return key($pairsToSplit);
    }
    return null;
}

function splitPair($number, $toSplit, &$orderRegister)
{
    $explodeParts = explode('-', $toSplit);
    $pairId = $explodeParts[0];
    $pairPosition = $explodeParts[1];
    $toSplitPair = $number[$pairId];
    $currentValue = $toSplitPair[$pairPosition];
    $newLeft = (int)floor($currentValue/2);
    $newRight = (int)ceil($currentValue/2);

    $newElement = [
        'id' => uniqid(),
        'level' => $toSplitPair['level'] + 1,
        'assign' => 'left',
        'left' => $newLeft,
        'right' => $newRight,
        'parent' => $toSplitPair['id'] ?? null,
        'child_position' => $pairPosition
    ];

    $number[$newElement['id']] = $newElement;

    $neighbors = getNeighbors($toSplitPair, $orderRegister);
    if ($pairPosition == 'left') {
        if (!is_null($neighbors['left'])) {
            // after left
            addAfter($newElement['id'] . '-right', $newElement['right'], $orderRegister, $neighbors['left']);
            addAfter($newElement['id'] . '-left', $newElement['left'], $orderRegister, $neighbors['left']);
        } else {
            //before all
            addBefore($newElement['id'] . '-right', $newElement['right'], $orderRegister);
            addBefore($newElement['id'] . '-left', $newElement['left'], $orderRegister);
        }
        remove([$toSplit], $orderRegister);
    } else {
        if (!is_null($neighbors['right'])) {
            // before right
            addBefore($newElement['id'] . '-left', $newElement['left'], $orderRegister, $neighbors['right']);
            addBefore($newElement['id'] . '-right', $newElement['right'], $orderRegister, $neighbors['right']);
        } else {
            // after all
            addAfter($newElement['id'] . '-left', $newElement['left'], $orderRegister);
            addAfter($newElement['id'] . '-right', $newElement['right'], $orderRegister);
        }
        remove([$toSplit], $orderRegister);
    }
    $toSplitPair[$pairPosition] = null;
    $number[$toSplitPair['id']] = $toSplitPair;
    return $number;
}

