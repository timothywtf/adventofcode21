<?php

namespace AOC\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Run extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $day = $input->getArgument('day');
        $puzzle = $input->getArgument('puzzle') ?? 'a';
        $run = __DIR__ . sprintf('/../%02d/%s.php', $day, $puzzle);
        if (!file_exists($run)) {
            $output->writeln("Bestaat niet, doe eens maken dan");
            return 1;
        }
        $input = file_get_contents(__DIR__ . sprintf('/../../input/%02d.txt', $day));

        require_once $run;
        return 1;
    }

    protected function configure()
    {
        $this
            ->setName('aoc:2021')
            ->addArgument('day', InputArgument::REQUIRED, 'd')
            ->addArgument('puzzle', InputArgument::OPTIONAL, 'p')
        ;
        parent::configure();
    }
}
