<?php

namespace AOC\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Init extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        for ($i = 1; $i <= 25; $i++) {
            if (!file_exists(__DIR__ . sprintf('/../../src/%02d', $i))) {
                @mkdir(__DIR__ . sprintf('/../../src/%02d', $i));
            }
            if (!file_exists(__DIR__ . sprintf('/../../src/%02d/a.php', $i))) {
                touch(__DIR__ . sprintf('/../../src/%02d/a.php', $i));
                file_put_contents(__DIR__ . sprintf('/../../src/%02d/a.php', $i), file_get_contents(__DIR__ . '/../../src/puzzle.php.dist'));
            }
            if (!file_exists(__DIR__ . sprintf('/../../src/%02d/b.php', $i))) {
                touch(__DIR__ . sprintf('/../../src/%02d/b.php', $i));
                file_put_contents(__DIR__ . sprintf('/../../src/%02d/b.php', $i), file_get_contents(__DIR__ . '/../../src/puzzle.php.dist'));
            }
            if (!file_exists(__DIR__ . sprintf('/../../input/%02d.txt', $i))) {
                touch(__DIR__ . sprintf('/../../input/%02d.txt', $i));
            }
        }
        return 1;
    }

    protected function configure()
    {
        $this->setName("aoc:2021:init");
        parent::configure();
    }
}
