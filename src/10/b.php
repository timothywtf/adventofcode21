<?php
/** @var array $input */
$input = explode(chr(10), $input);
$incomplete = [];
foreach ($input as $line) {
    $score = 0;
    $expected = [];
    foreach (str_split($line) as $char) {
        switch ($char) {
            case '(' : $expected[] = ')';
                break;
            case ')' : $lastExpected = array_pop($expected);
                if ($lastExpected != ')') {
                    $score += 3;
                    continue 2;
                }
                break;
            case '[' : $expected[] = ']';
                break;
            case ']' : $lastExpected = array_pop($expected);
                if ($lastExpected != ']') {
                    $score += 57;
                    continue 2;
                }
                break;
            case '{' : $expected[] = '}';
                break;
            case '}' : $lastExpected = array_pop($expected);
                if ($lastExpected != '}') {
                    $score += 1197;
                    continue 2;
                }
                break;
            case '<' : $expected[] = '>';
                break;
            case '>' : $lastExpected = array_pop($expected);
                if ($lastExpected != '>') {
                    $score += 25137;
                    continue 2;
                }
                break;
        }
    }
    if ($score == 0) {
        $incomplete[] = $line;
    }
}

$scores = [];
foreach ($incomplete as $incompleteLine) {
    $score = 0;
    $expected = [];
    foreach (array_reverse(str_split($incompleteLine)) as $char) {
        switch ($char) {
            case '(' :
                if (empty($expected)) {
                    $score *= 5;
                    $score += 1;
                } else {
                    $lastExpected = array_pop($expected);
                }
                break;
            case ')' : $expected[] = '(';
                break;
            case '[' :
                if (empty($expected)) {
                    $score *= 5;
                    $score += 2;
                } else {
                    $lastExpected = array_pop($expected);
                }
                break;
            case ']' : $expected[] = '[';
                break;
            case '{' :
                if (empty($expected)) {
                    $score *= 5;
                    $score += 3;
                } else {
                    $lastExpected = array_pop($expected);
                }
                break;
            case '}' : $expected[] = '{';
                break;
            case '<' :
                if (empty($expected)) {
                    $score *= 5;
                    $score += 4;
                } else {
                    $lastExpected = array_pop($expected);
                }
                break;
            case '>' : $expected[] = '<';
                break;
        }
    }
    $scores[] = $score;
}
sort($scores);
dd($scores[floor(count($scores)/2)]);
