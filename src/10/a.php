<?php
/** @var string $input */
$input = explode(chr(10), $input);

$score = 0;
foreach ($input as $line) {
    $expected = [];
    foreach (str_split($line) as $char) {
        switch ($char) {
            case '(' : $expected[] = ')';
                break;
            case ')' : $lastExpected = array_pop($expected);
                if ($lastExpected != ')') {
                    $score += 3;
                    continue 2;
                }
                break;
            case '[' : $expected[] = ']';
                break;
            case ']' : $lastExpected = array_pop($expected);
                if ($lastExpected != ']') {
                    $score += 57;
                    continue 2;
                }
                break;
            case '{' : $expected[] = '}';
                break;
            case '}' : $lastExpected = array_pop($expected);
                if ($lastExpected != '}') {
                    $score += 1197;
                    continue 2;
                }
                break;
            case '<' : $expected[] = '>';
                break;
            case '>' : $lastExpected = array_pop($expected);
                if ($lastExpected != '>') {
                    $score += 25137;
                    continue 2;
                }
                break;
        }
    }
}
dd($score);