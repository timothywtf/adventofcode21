<?php
/** @var string $input */
//dd($input);

$input = <<<INPUT
--- scanner 0 ---
404,-588,-901
528,-643,409
-838,591,734
390,-675,-793
-537,-823,-458
-485,-357,347
-345,-311,381
-661,-816,-575
-876,649,763
-618,-824,-621
553,345,-567
474,580,667
-447,-329,318
-584,868,-557
544,-627,-890
564,392,-477
455,729,728
-892,524,684
-689,845,-530
423,-701,434
7,-33,-71
630,319,-379
443,580,662
-789,900,-551
459,-707,401

--- scanner 1 ---
686,422,578
605,423,415
515,917,-361
-336,658,858
95,138,22
-476,619,847
-340,-569,-846
567,-361,727
-460,603,-452
669,-402,600
729,430,532
-500,-761,534
-322,571,750
-466,-666,-811
-429,-592,574
-355,545,-477
703,-491,-529
-328,-685,520
413,935,-424
-391,539,-444
586,-435,557
-364,-763,-893
807,-499,-711
755,-354,-619
553,889,-390

--- scanner 2 ---
649,640,665
682,-795,504
-784,533,-524
-644,584,-595
-588,-843,648
-30,6,44
-674,560,763
500,723,-460
609,671,-379
-555,-800,653
-675,-892,-343
697,-426,-610
578,704,681
493,664,-388
-671,-858,530
-667,343,800
571,-461,-707
-138,-166,112
-889,563,-600
646,-828,498
640,759,510
-630,509,768
-681,-892,-333
673,-379,-804
-742,-814,-386
577,-820,562

--- scanner 3 ---
-589,542,597
605,-692,669
-500,565,-823
-660,373,557
-458,-679,-417
-488,449,543
-626,468,-788
338,-750,-386
528,-832,-391
562,-778,733
-938,-730,414
543,643,-506
-524,371,-870
407,773,750
-104,29,83
378,-903,-323
-778,-728,485
426,699,580
-438,-605,-362
-469,-447,-387
509,732,623
647,635,-688
-868,-804,481
614,-800,639
595,780,-596

--- scanner 4 ---
727,592,562
-293,-554,779
441,611,-461
-714,465,-776
-743,427,-804
-660,-479,-426
832,-632,460
927,-485,-438
408,393,-506
466,436,-512
110,16,151
-258,-428,682
-393,719,612
-211,-452,876
808,-476,-593
-575,615,604
-485,667,467
-680,325,-822
-627,-443,-432
872,-547,-609
833,512,582
807,604,487
839,-516,451
891,-625,532
-652,-548,-490
30,-46,-14
INPUT;

$scannerInputs = explode(PHP_EOL.PHP_EOL, $input);
$scanners = [];

foreach ($scannerInputs as $scanner) {
    $scannerLines = explode(chr(10), $scanner);
    preg_match("/.*(\d+).*/", array_shift($scannerLines), $matches);
    $scanners[$matches[1]] = array_map(function ($scannerLine) {return explode(',', $scannerLine);}, $scannerLines);
}

function simplifyScanner($scanner) {
    return array_map(
        'simplifyCoord',
        $scanner
    );
}

function simplifyCoord($coord) {
    return implode(',', $coord);
}



//dump($referenceScanner);
//dump($testScanner);
//die;

//dd($scanner0[9]);
//dd($scanner1[0]);

$permutations = [];
foreach ([0,1,2] as $baseDimension) {
    foreach ([-1,1] as $flip) {
        foreach ([-1,1] as $flipFirstOther) {
            foreach ([-1,1] as $flipSecondOther) {
                $permutations[] = [$baseDimension, $flip, $flipFirstOther, $flipSecondOther];
            }
        }
    }
}

//$simplifiedScanner1 = simplifyScanner($testScanner);

$testRecord = [];


$found = [];

function calcDiffs($scanner) {
    $scannerTwice = $scanner;
    foreach ($scanner as $key => $coord) {
        foreach ($scannerTwice as $secondCoord) {
            if ($coord == $secondCoord) {
                continue;
            }
            $todo[] = [
                $coord,
                $secondCoord
            ];
        }
        unset($scannerTwice[$key]);
    }

    $results = [];
    while (!empty($todo)) {
        $pair = array_shift($todo);
        $currentBeacon = $pair[0];
        $lastBeacon = $pair[1];
        $diff[0] = $currentBeacon[0] - $lastBeacon[0];
        $diff[1] = $currentBeacon[1] - $lastBeacon[1];
        $diff[2] = $currentBeacon[2] - $lastBeacon[2];
        $result['a'] = $currentBeacon;
        $result['b'] = $lastBeacon;
        $result['diff'] = $diff;
        $result['a_simple'] = simplifyCoord($currentBeacon);
        $result['b_simple'] = simplifyCoord($lastBeacon);
        $result['diff_simple'] = simplifyCoord($diff);
//        $diff[0] = ($currentBeacon[0] > $lastBeacon[0]) ? $currentBeacon[0] - $lastBeacon[0] : $lastBeacon[0] - $currentBeacon[0];
//        $diff[1] = ($currentBeacon[1] > $lastBeacon[1]) ? $currentBeacon[1] - $lastBeacon[1] : $lastBeacon[1] - $currentBeacon[1];
//        $diff[2] = ($currentBeacon[2] > $lastBeacon[2]) ? $currentBeacon[2] - $lastBeacon[2] : $lastBeacon[2] - $currentBeacon[2];
        $results[] = $result;
    }
    return $results;
}

$referenceScanner = $scanners[1];
$testScanner = $scanners[4];

$scanner0Diffs = calcDiffs($referenceScanner);
$scanner1Diffs = calcDiffs($testScanner);

function searchDiffMap($diff, $diffMap)
{
    $foundDiffs = array_filter($diffMap, function ($diffMapEntry) use ($diff) {
        return $diffMapEntry['diff_simple'] == $diff;
    });
    if (count($foundDiffs) > 1) {
        echo "more than one" . PHP_EOL;
    }
    return reset($foundDiffs);
}

$trace = [];
$testDerp = [];
foreach ($scanner1Diffs as $scanner1Diff) {
    foreach ($permutations as $permutation) {
        $base = array_shift($permutation);
        $orientation = $permutation;
        $perm = getSimplePerspective($scanner1Diff['diff'], $base, $orientation);
        $findCorrespondingDiff = searchDiffMap(simplifyCoord($perm), $scanner0Diffs);

        if (!empty($findCorrespondingDiff)) {
            var_dump(sprintf('%s: %s, %s, %s', $base, $orientation[0], $orientation[1], $orientation[2]));
//            $trace[simplifyCoord($scanner1Diff['b'])] = simplifyCoord($findCorrespondingDiff['a']);
//            $trace[simplifyCoord($scanner1Diff['a'])] = simplifyCoord($findCorrespondingDiff['b']);
            break;
        }
    }
}
dd($trace);
die('end');

foreach ($referenceScanner as $key => $coord) {
    foreach ($referenceScannerSecond as $secondCoord) {
        if ($coord == $secondCoord) {
            continue;
        }
        $abCoordX = abs($coord[0]);
        $abCoordY = abs($coord[1]);
        $abCoordZ = abs($coord[2]);
        $abSecondCoordX = abs($secondCoord[0]);
        $abSecondCoordY = abs($secondCoord[1]);
        $abSecondCoordZ = abs($secondCoord[2]);
        $coordDiffence[0] = ($abCoordX > $abSecondCoordX) ? $abCoordX - $abSecondCoordX : $abSecondCoordX - $abCoordX;
        $coordDiffence[1] = ($abCoordY > $abSecondCoordY) ? $abCoordY - $abSecondCoordY : $abSecondCoordY - $abCoordY;
        $coordDiffence[2] = ($abCoordZ > $abSecondCoordZ) ? $abCoordZ - $abSecondCoordZ : $abSecondCoordZ - $abCoordZ;

        foreach ($permutations as $permutation) {
            $base = array_shift($permutation);
            $orientation = $permutation;
            $perm = getSimplePerspective($coordDiffence, $base, $orientation);

            foreach ($testScanner as $scannerKey => $scanner1Item) {
                $trans[0] = $scanner1Item[0] + $perm[0];
                $trans[1] = $scanner1Item[1] + $perm[1];
                $trans[2] = $scanner1Item[2] + $perm[2];
                if (in_array(simplifyCoord($trans), $simplifiedScanner1)) {
                    $found[simplifyCoord($trans)] = simplifyCoord($trans);
                    $found[simplifyCoord($scanner1Item)] = simplifyCoord($scanner1Item);
//                    $scannerTest[0] = ($abCoordX > $abSecondCoordX) ? $coord[0] + $trans[0] : $coord[0] - $trans[0];
//                    $scannerTest[1] = ($abCoordY > $abSecondCoordY) ? $coord[1] + $trans[1] : $coord[1] - $trans[1];
//                    $scannerTest[2] = ($abCoordZ > $abSecondCoordZ) ? $coord[2] + $trans[2] : $coord[2] - $trans[2];
//                    dump(simplifyCoord($scannerTest));
                    $testRecord[] = [
                        'coord' => simplifyCoord($coord),
                        'secondCoord' => simplifyCoord($secondCoord),
                        'trans' => simplifyCoord($trans),
                        'scanner1Item' => simplifyCoord($scanner1Item),
                        'xtest' => ($abCoordX > $abSecondCoordX),
                        'ytest' => ($abCoordY > $abSecondCoordY),
                        'ztest' => ($abCoordZ > $abSecondCoordZ),
                        'transPers' => simplifyCoord(getSimplePerspective($trans, $base, $orientation)),
                        'twicetwicePers' => simplifyCoord(getSimplePerspective($scanner1Item, $base, $orientation)),
                        'base' => $base,
                        'orientation' => $orientation
                    ];
                    break 2;
                }
            }
        }
    }
    unset($referenceScannerSecond[$key]);
}

dump($found);
dd($testRecord);
dump(array_count_values($found));
if (count($found) >= 12) {
    dd('12');
}

dd('not 12');

$simpleTestA = implode(',', $testA);


foreach ($permutations as $permutation) {
    $foundCount = 0;
    $base = array_shift($permutation);
    $orientation = $permutation;

//    dump($testB);
    $perspectiveB = getSimplePerspective($testB, $base, $orientation);
//    dump($perspectiveB);
    $deltaX = $testA[0] + $perspectiveB[0];
    $deltaY = $testA[1] + $perspectiveB[1];
    $deltaZ = $testA[2] + $perspectiveB[2];

    $transformedC[0] = $testC[0] + $deltaX;
    $transformedC[1] = $testC[1] + $deltaY;
    $transformedC[2] = $testC[2] + $deltaZ;
    dump($transformedC);

}
dump($testA);
dump($testB);
dump($testC);
dump($testD);
die;
function getSimplePerspective($coord, int $base, array $orientation)
{
    $possibleCoords = [0, 1, 2];
    $firstDimension = $coord[$base];
    unset($possibleCoords[$base]);
    $firstDimension *= $orientation[0];
    $newCoord[$base] = $firstDimension;
    $firstOtherPosition = array_shift($possibleCoords);
    $secondOtherPosition = array_shift($possibleCoords);
    $firstOtherCorrected = $coord[$firstOtherPosition] * $orientation[1];
    $secondOtherCorrected = $coord[$secondOtherPosition] * $orientation[2];
    if ($orientation[2] == 1) {
        $newCoord[$firstOtherPosition] = $firstOtherCorrected;
        $newCoord[$secondOtherPosition] = $secondOtherCorrected;
    } else {
        $newCoord[$firstOtherPosition] = $secondOtherCorrected;
        $newCoord[$secondOtherPosition] = $firstOtherCorrected;
    }

    return $newCoord;
}

function getPerspectives($coord, $base = null, $orientation = [])
{
    if (!empty($orientation) && count($orientation) !== 3) {
        throw new \Exception('Orientation invalid, ' . implode(",", $orientation));
    }
    if (!is_null($base) xor !empty($orientation)) {
        throw new \Exception('Either orientation or base was provided, should be both or neither');
    }
    $baseDimensions = (!is_null($base)) ? [$base] : [0, 1, 2];
    $baseFlips = (!is_null($base)) ? [$orientation[0]] : [1, -1];
    $firstOtherFlips = (!is_null($base)) ? [$orientation[1]] : [1, -1];
    $secondOtherFlips = (!is_null($base)) ? [$orientation[2]] : [1, -1];

    $permutations = [];
    foreach ($baseDimensions as $baseDimension) {
        $firstDimension = $coord[$baseDimension];
        foreach ($baseFlips as $flip) {
            $possibleCoords = [0, 1, 2];
            unset($possibleCoords[$baseDimension]);
            $firstDimension *= $flip;
            // rotate first other
            $firstOtherPosition = array_shift($possibleCoords);
            $secondOtherPosition = array_shift($possibleCoords);
            foreach ($firstOtherFlips as $flipFirstOther) {
                $firstOtherCorrected = $coord[$firstOtherPosition] * $flipFirstOther;
                // rotate second first other
                foreach ($secondOtherFlips as $flipSecondOther) {
                    $secondOtherCorrected = $coord[$secondOtherPosition] * $flipSecondOther;
                    $newCoord = [];
                    $newCoord[$baseDimension] = $firstDimension;
                    $orientation[0] = $flip;
                    if ($flipSecondOther == 1) {
                        $newCoord[$firstOtherPosition] = $firstOtherCorrected;
                        $newCoord[$secondOtherPosition] = $secondOtherCorrected;
                    } else {
                        $newCoord[$firstOtherPosition] = $secondOtherCorrected;
                        $newCoord[$secondOtherPosition] = $firstOtherCorrected;
                    }
                    $orientation[1] = $flipFirstOther;
                    $orientation[2] = $flipSecondOther;

                    $newCoord['simple'] = implode(',', $newCoord);
                    $newCoord['orientation'] = $orientation;
                    $newCoord['base'] = $baseDimension;
                    $permutations[] = $newCoord;
                }
            }
        }
    }
    return $permutations;
}


$testCoord = [8, 13, -5];
$getPerspectives = getPerspectives($testCoord);
dump(array_map(function ($permutation) {
    $coordPart = $permutation['simple'];
    $coordPart .= ' (' .  implode(', ', $permutation['orientation']) . ')';
    $coordPart .= sprintf(' (base: %s)', $permutation['base']);
    return $coordPart;
}, $getPerspectives));

$test = $getPerspectives[21];
//dump($forteenth);


$getPerspectives2 = getPerspectives($testCoord, $test['base'], $test['orientation']);
dump(array_map(function ($permutation) {
    $coordPart = $permutation['simple'];
    $coordPart .= ' (' .  implode(', ', $permutation['orientation']) . ')';
    $coordPart .= sprintf(' (base: %s)', $permutation['base']);
    return $coordPart;
}, $getPerspectives2));

dump(getSimplePerspective($testCoord, $test['base'], $test['orientation']));