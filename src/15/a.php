<?php
use Symfony\Component\Console\Output\OutputInterface;
/** @var OutputInterface $output */
/** @var string $input */
//$input = <<<DEBUG
//1163751742
//1381373672
//2136511328
//3694931569
//7463417111
//1319128137
//1359912421
//3125421639
//1293138521
//2311944581
//DEBUG;

$inputList = [];
foreach (explode(chr(10), $input) as $yAxis => $item) {
    $elements = str_split($item);
    foreach ($elements as $xAxis => $risk) {
        $inputList[$yAxis][$xAxis] = new \AOC\Path\Node($xAxis, $yAxis, $risk);
    }
}
$xRange = count($inputList[0]) - 1;
$yRange = count($inputList) - 1;

for ($x = 0; $x < count($inputList); $x++) {
    for ($y = 0; $y < count($inputList[$x]); $y++) {
        /** @var \AOC\Path\Node $currentNode */
        $currentNode = $inputList[$x][$y];
        for ($xDelta = -1; $xDelta <= 1; $xDelta++) {
            for ($yDelta = -1; $yDelta <= 1; $yDelta++) {
                if ($xDelta == 0 xor $yDelta == 0) {
                    $adjacentNode = $inputList[$x+$xDelta][$y+$yDelta] ?? null;
                    if ($adjacentNode instanceof \AOC\Path\Node) {
                        $currentNode->addAdjacentNode($adjacentNode);
                    }
                }
            }
        }
    }
}

function test($inputList, OutputInterface $output, array $shortestPathNodes = []) {
    for ($x = 0; $x < count($inputList); $x++) {
        $line = '';
        for ($y = 0; $y < count($inputList[$x]); $y++) {
            /** @var \AOC\Path\Node $currentNode */
            $currentNode = $inputList[$x][$y];
            $line .= (in_array($currentNode->getUniqueNodeId(), $shortestPathNodes))
                ? sprintf("<info>%s</info>", $currentNode->getRisk())
                : $currentNode->getRisk();
        }
        $output->writeln($line);
    }
}

$startNode = $inputList[0][0];
$endNode = $inputList[$xRange][$yRange];
test($inputList, $output);
$output->writeln("");
$cave = new \AOC\Path\ManhattanCave();
$aStar = new \JMGQ\AStar\AStar($cave);
$solution = $aStar->run($startNode, $endNode);
$simpleSolution = array_map(function (\AOC\Path\Node $node) {return $node->getUniqueNodeId();}, $solution);
array_shift($solution);
$riskCollection = array_map(function (\AOC\Path\Node $node) {return $node->getRisk();}, $solution);
test($inputList, $output, $simpleSolution);
dump(array_sum($riskCollection));
die;
