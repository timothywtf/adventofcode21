<?php
$start = microtime(true);
use Symfony\Component\Console\Output\OutputInterface;
/** @var OutputInterface $output */
/** @var string $input */
//$input = <<<DEBUG
//1163751742
//1381373672
//2136511328
//3694931569
//7463417111
//1319128137
//1359912421
//3125421639
//1293138521
//2311944581
//DEBUG;

$inputList = [];
foreach (explode(chr(10), $input) as $yAxis => $item) {
    $elements = str_split($item);
    foreach ($elements as $xAxis => $risk) {
        $inputList[$yAxis][$xAxis] = new \AOC\Path\Node($xAxis, $yAxis, $risk);
    }
}

function test($inputList, OutputInterface $output, array $shortestPathNodes = []) {
    for ($x = 0; $x < count($inputList); $x++) {
        $line = '';
        for ($y = 0; $y < count($inputList[$x]); $y++) {
            /** @var \AOC\Path\Node $currentNode */
            $currentNode = $inputList[$x][$y];
            $line .= (in_array($currentNode->getUniqueNodeId(), $shortestPathNodes))
                ? sprintf("<info>%s</info>", $currentNode->getRisk())
                : $currentNode->getRisk();
        }
        $output->writeln($line);
    }
}
function calcRisk($risk, $riskExtension)
{
    $newRisk = $risk + $riskExtension;
    if ($newRisk >= 10) {
        $newRisk -= 9;
    }
    return $newRisk;
}

$graph = new \Fhaculty\Graph\Graph();
$xRange = count($inputList[0]);
$yRange = count($inputList);

$fiveByFive = [];
for ($xExtension = 0; $xExtension < 5; $xExtension++) {
    for ($yExtension = 0; $yExtension < 5; $yExtension++) {
        $riskExtension = $xExtension + $yExtension;
        foreach ($inputList as $y => $row) {
            /** @var \AOC\Path\Node $node */
            $currentY = $y + ($yRange * $yExtension);
            foreach ($row as $x => $node) {
                $currentX = $x + ($xRange * $xExtension);
                $fiveByFive[$currentY][$currentX] = new \AOC\Path\Node(
                    $currentX,
                    $currentY,
                    calcRisk($node->getRisk(), $riskExtension)
                );
            }
        }
    }
}

$xRange = count($fiveByFive[0]) - 1;
$yRange = count($fiveByFive) - 1;

for ($x = 0; $x < count($fiveByFive); $x++) {
    for ($y = 0; $y < count($fiveByFive[$x]); $y++) {
        /** @var \AOC\Path\Node $currentNode */
        $currentNode = $fiveByFive[$x][$y];
        for ($xDelta = -1; $xDelta <= 1; $xDelta++) {
            for ($yDelta = -1; $yDelta <= 1; $yDelta++) {
                if ($xDelta == 0 xor $yDelta == 0) {
                    $adjacentNode = $fiveByFive[$x+$xDelta][$y+$yDelta] ?? null;
                    if ($adjacentNode instanceof \AOC\Path\Node) {
                        $currentNode->addAdjacentNode($adjacentNode);
                    }
                }
            }
        }
    }
}


$startNode = $fiveByFive[0][0];
$endNode = $fiveByFive[$xRange][$yRange];
//test($fiveByFive, $output);
$output->writeln("");
$cave = new \AOC\Path\ManhattanCave();
$aStar = new \JMGQ\AStar\AStar($cave);
$solution = $aStar->run($startNode, $endNode);
$simpleSolution = array_map(function (\AOC\Path\Node $node) {return $node->getUniqueNodeId();}, $solution);
array_shift($solution);
$riskCollection = array_map(function (\AOC\Path\Node $node) {return $node->getRisk();}, $solution);
test($fiveByFive, $output, $simpleSolution);
dump(array_sum($riskCollection));

$time_elapsed_secs = microtime(true) - $start;
dd($time_elapsed_secs);
//2935