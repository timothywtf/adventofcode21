<?php
/** @var string $input */
$octos = [];
$lines = explode(chr(10), $input);
foreach ($lines as $line) {
    $octos[] = str_split($line);
}

$test = function ($octos, $message = '') {
    echo $message . PHP_EOL;
    foreach ($octos as $octoRow) {
        echo implode('', array_map(function ($octo) {return sprintf("%01s", $octo);}, $octoRow)) . PHP_EOL;
    }
    echo PHP_EOL . PHP_EOL;
};

//$test($octos, 'begin');

$flash = function (&$octos, $i, &$stepRegister, &$flashes) use ($test) {
    foreach ($octos as $yKey => $octoY) {
        foreach ($octoY as $xKey => $lightLevel) {
            if ($lightLevel > 9) {
                $registerKey = $xKey . ',' . $yKey;
                if (!isset($stepRegister[$registerKey])) {
                    $flashes++;
                    for ($xDelta = -1; $xDelta <= 1; $xDelta++) {
                        for ($yDelta = -1; $yDelta <= 1; $yDelta++) {
                            if ($xDelta == 0 && $yDelta == 0) {
                                continue;
                            }
                            if (isset($octos[$yKey+$yDelta][$xKey+$xDelta])) {
                                $octos[$yKey+$yDelta][$xKey+$xDelta] += 1;
                            }
                        }
                    }
                    $stepRegister[$registerKey] = $registerKey;
                }
            }
        }
    }

//    $test($octos, 'na flash');

    $tenCount = 0;
    foreach ($octos as $yKey => $octoY) {
        foreach ($octoY as $xKey => $lightLevel) {
            if ($lightLevel >= 10) {
                $tenCount++;
            }
        }
    }

    return $tenCount;
};



$range = 100;
$flashes = 0;
for ($i = 1; $i <= $range; $i++) {
//    echo $i . PHP_EOL;
    $stepRegister = [];
    foreach ($octos as $yKey => $octoY) {
        foreach ($octoY as $xKey => $lightLevel) {
            $octos[$yKey][$xKey] += 1;
        }
    }

//    $test($octos, 'na +1');
    $shouldRun = true;
    $lastStepRegisterCount = 0;
    while ($shouldRun) {
        $tenCount = $flash($octos, $i, $stepRegister, $flashes);
        $currentStepRegisterCount = count($stepRegister);
        $shouldRun = $tenCount > 0 && $currentStepRegisterCount != $lastStepRegisterCount;
        $lastStepRegisterCount = $currentStepRegisterCount;
    }
    foreach ($octos as $yKey => $octoY) {
        foreach ($octoY as $xKey => $lightLevel) {
            if ($lightLevel >= 10) {
                $octos[$yKey][$xKey] = 0;
            }
        }
    }

//    $test($octos, 'na normalisatie ');
//    echo "+++++++++" . PHP_EOL . PHP_EOL;
}
dd($flashes);

