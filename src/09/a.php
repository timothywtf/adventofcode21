<?php
/** @var string $input */
$rows = explode(chr(10), $input);
$coords = [];
$y = 0;
foreach ($rows as $row) {
    $coords[$y++] = array_map('intval', str_split($row));
}
$riskLevels = [];

for ($yCoord = 0; $yCoord < count($coords); $yCoord++) {
    for ($xCoord = 0; $xCoord < count($coords[$yCoord]); $xCoord++) {
        $north = (isset($coords[$yCoord-1])) ? $coords[$yCoord-1][$xCoord] : null;
        $east = (isset($coords[$yCoord][$xCoord+1])) ? $coords[$yCoord][$xCoord+1] : null;
        $south = (isset($coords[$yCoord+1])) ? $coords[$yCoord+1][$xCoord] : null;
        $west = (isset($coords[$yCoord][$xCoord-1])) ? $coords[$yCoord][$xCoord-1] : null;
        $heights = array_filter([$north, $east, $south, $west], function ($coord) {return is_int($coord);});
        if ($coords[$yCoord][$xCoord] < min($heights)) {
            $riskLevels[] = $coords[$yCoord][$xCoord] + 1;
        }
    }
}

dd(array_sum($riskLevels));
