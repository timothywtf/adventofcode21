<?php
/** @var string $input */
$rows = explode(chr(10), $input);
$coords = [];
$y = 0;
foreach ($rows as $row) {
    $coords[$y++] = array_map('intval', str_split($row));
}
$lowPoints = [];

for ($yCoord = 0; $yCoord < count($coords); $yCoord++) {
    for ($xCoord = 0; $xCoord < count($coords[$yCoord]); $xCoord++) {
        $north = (isset($coords[$yCoord-1])) ? $coords[$yCoord-1][$xCoord] : null;
        $east = (isset($coords[$yCoord][$xCoord+1])) ? $coords[$yCoord][$xCoord+1] : null;
        $south = (isset($coords[$yCoord+1])) ? $coords[$yCoord+1][$xCoord] : null;
        $west = (isset($coords[$yCoord][$xCoord-1])) ? $coords[$yCoord][$xCoord-1] : null;
        $heights = array_filter([$north, $east, $south, $west], function ($coord) {return is_int($coord);});
        if ($coords[$yCoord][$xCoord] < min($heights)) {
            $lowPoints[] = [$yCoord, $xCoord];
        }
    }
}

$fillBasin = function ($lowPoint) use ($coords) {
    $seen = [];
    $toCheck = [$lowPoint];
    $basinSize = 1;
    while (!empty($toCheck)) {
        $coord = array_shift($toCheck);
        $seen[] = $coord[0]  .':'. $coord[1];
        $north = (isset($coords[$coord[0]-1][$coord[1]])) ? $coords[$coord[0]-1][$coord[1]] : null;
        $possibleNextNordCoord = [$coord[0] - 1, $coord[1]];
        if ($north != null
            && $north != 9
            && !in_array($possibleNextNordCoord[0]  .':'. $possibleNextNordCoord[1], $seen)
            && !isset($toCheck[$possibleNextNordCoord[0]  .':'. $possibleNextNordCoord[1]])
        ) {
            $toCheck[$possibleNextNordCoord[0]  .':'. $possibleNextNordCoord[1]] = $possibleNextNordCoord;
            $basinSize++;
        }
        $east = (isset($coords[$coord[0]][$coord[1]+1])) ? $coords[$coord[0]][$coord[1]+1] : null;
        $possibleNextEastCoord = [$coord[0], $coord[1] + 1];
        if ($east != null
            && $east != 9
            && !in_array($possibleNextEastCoord[0]  .':'. $possibleNextEastCoord[1], $seen)
            && !isset($toCheck[$possibleNextEastCoord[0]  .':'. $possibleNextEastCoord[1]])
        ) {
            $toCheck[$possibleNextEastCoord[0]  .':'. $possibleNextEastCoord[1]] = $possibleNextEastCoord;
            $basinSize++;
        }
        $south = (isset($coords[$coord[0]+1][$coord[1]])) ? $coords[$coord[0]+1][$coord[1]] : null;
        $possibleNextSouthCoord = [$coord[0] + 1, $coord[1]];
        if ($south != null
            && $south != 9
            && !in_array($possibleNextSouthCoord[0]  .':'. $possibleNextSouthCoord[1], $seen)
            && !isset($toCheck[$possibleNextSouthCoord[0]  .':'. $possibleNextSouthCoord[1]])
        ) {
            $toCheck[$possibleNextSouthCoord[0]  .':'. $possibleNextSouthCoord[1]] = $possibleNextSouthCoord;
            $basinSize++;
        }
        $west = (isset($coords[$coord[0]][$coord[1]-1])) ? $coords[$coord[0]][$coord[1]-1] : null;
        $possibleNextWestCoord = [$coord[0], $coord[1] - 1];
        if ($west != null
            && $west != 9
            && !in_array($possibleNextWestCoord[0]  .':'. $possibleNextWestCoord[1], $seen)
            && !isset($toCheck[$possibleNextWestCoord[0]  .':'. $possibleNextWestCoord[1]])
        ) {
            $toCheck[$possibleNextWestCoord[0]  .':'. $possibleNextWestCoord[1]] = $possibleNextWestCoord;
            $basinSize++;
        }
    }
    return $basinSize;
};

$basins = array_map($fillBasin, $lowPoints);
rsort($basins);
dd(array_product(array_slice($basins, 0, 3)));
