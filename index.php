<?php
require_once __DIR__ . '/vendor/autoload.php';
use Symfony\Component\Console\Application;
$application = new Application();
$application->add(new \AOC\Command\Run());
$application->add(new \AOC\Command\Init());
$application->run();
?>

